# SPDX-License-Identifier: CC-BY-SA-4.0
# SPDX-FileCopyrightText: © 2022 Olaf Meeuwissen

source "https://rubygems.org"
ruby ">= 2.5"

gem "jekyll", "~> 4.2"
if Gem::Version.new(RUBY_VERSION) >= Gem::Version.new("3.0.0")
  gem "webrick", "~> 1.7"
end

group :jekyll_themes do
  gem "minima", "~> 2.5"
end

group :jekyll_plugins do
  gem "jekyll-feed", "~> 0.12"
end

# Windows and JRuby do not include zoneinfo files, so bundle the
# tzinfo-data gem and lock the associated library's version.
platforms :mingw, :x64_mingw, :mswin, :jruby do
  gem "tzinfo", "~> 1.2"
  gem "tzinfo-data"
end

# Performance-booster for watching directories on Windows
gem "wdm", "~> 0.1.1", :platforms => [:mingw, :x64_mingw, :mswin]
