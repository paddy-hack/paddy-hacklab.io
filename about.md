---
layout: page
title: About
permalink: /about/
---

This is a collection of posts and pages by a geek.  Information will
go stale, some of it sooner rather than later.  Understanding may be
muddled, descriptions unclear and opinions unbalanced.

Therefore: "reader beware" (or as the old Romans used to say "caveat
lector").
