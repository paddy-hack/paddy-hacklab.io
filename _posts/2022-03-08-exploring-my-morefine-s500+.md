---
SPDX-License-Identifier: CC-BY-SA-4.0
SPDX-FileCopyrightText: © 2022 Olaf Meeuwissen
layout: post
title: "Exploring My MOREFINE S500+"
---

> This post documents stuff I checked on my [MOREFINE S500+ Mini PC]({% link _posts/2022-02-28-morefine-s500+-mini-pc.md %})
> begin December 2021.

The pre-installed Ubuntu booted fine, but the login screen was in
Chinese, only.  At about the same time I found that out, I also
realized that I would have a hard time entering a user name and
password.  And not just because I didn't know the defaults at that
point.  How would I pair my Bluetooth-only keyboard?  At least my
Bluetooth-capable mouse could still be connected through its USB
wireless dongle but unless pairing Bluetooth devices on the login
screen is supported or there is some kind of virtual keyboard, all
operable with a mouse only, how would I even log in?

I switched to Plan B.  I created a [Devuan Chimaera Desktop Live](https://files.devuan.org/devuan_chimaera/desktop-live/) USB
stick that I could boot off of and arranged a USB wired keyboard and
mouse.  Pressing the `ESC` key real quick, I was able to get at the
pre-installed Ubuntu's (Chinese) GRUB menu.

![Ubuntu GRUB menu in Chinese](/assets/images/chinese-grub-menu.png)

Selecting the `UEFI Firmware Settings`, I got at the `American Megatrends`
BIOS (in English), opened the `Save & Exit` tab and told the BIOS to
boot off my Devuan USB stick.  You can set `Boot` order explicitly, but
the `Save & Exit` menu allows for one time overrides.

> On the `Boot` tab, I did change the `Setup Prompt Timeout` from `0`
> seconds to a few seconds to make getting to the BIOS a bit easier.

Devuan Chimaera Desktop Live booted without any problems.  I logged in
using the credentials from the accompanying [README](https://files.devuan.org/devuan_chimaera/desktop-live/README_desktop-live.txt) and set about
exploring.

![Devuan Chimaera Desktop Live ](/assets/images/devuan-chimaera-desktop-live.png)

# First Check: Hardware Spec

I confirmed the CPU by looking at `/proc/cpuinfo`.  With 16 cores, that
file is a bit long so commands like

``` sh
grep 'model name' /proc/cpuinfo | wc -l
grep 'model name' /proc/cpuinfo | sort -u
```

will be helpful in counting actual number of cores and listing the
core model(s).  In my case, the results, as expected, were

``` console
16
model name	: AMD Ryzen 9 5900HX with Radeon Graphics
```

I had asked for 64GB DDR4 RAM.  The part I really cared about was the
amount of memory so I checked that with

``` console
$ cat /proc/meminfo | grep MemTotal
MemTotal:       65312980 kB
```

There is always some discrepancy between base-10 (10^3, 10^6, etc) and
base-2 (2^10, 2^20, etc) sizes but the number in the output is in the
64GB ballpark alright.

I had also asked for `2TB NVMe SSD` and used `fdisk -l` to check that.
Note that this needs administrative (`root`) privileges.  The part of
the (abbreviated) output below confirmed the storage was as ordered.

``` console
# fdisk -l
Disk /dev/nvme0n1: 1.86 TiB, 2048408248320 bytes, 4000797360 sectors
Disk model: NVME SSD 2TB
[...]
Device           Start        End    Sectors Size Type
/dev/nvme0n1p1    2048    1050623    1048576 512M EFI System
/dev/nvme0n1p2 1050624 4000796671 3999746048 1.9T Linux filesystem
```

Again, base-2 and base-10 size differences need to be accounted for
but 1.86TiB is about 2TB.

Finaly, the Intel AX200 option I added to my order can be checked with

``` console
$ lspci | grep AX200
03:00.0 Network controller: Intel Corporation Wi-Fi 6 AX200 (rev 1a)
```

Well, all the hardware bits were as I had ordered.  Next.

# Second Check: Pre-Installed Ubuntu

We're running off a live USB stick so the pre-installed Ubuntu is to
be found on `/dev/nvme0n1p2` based on the `fdisk` output above.  Let's do
a one-off mount of that partition on `/mnt`

``` sh
sudo mount /dev/nvme0n1p2 /mnt
```

and poke around the place below `/mnt`.

The `/mnt/etc/os-release` file claims `Ubuntu 20.04.3 LTS (Focal Fossa)`
is installed.  A Long Term Support release, good!  About 8.8GB of disk
space was used by the pre-installed distribution.  That includes a
2.1GB `/mnt/swapfile` that will be used for swapping purposes when
booting the pre-installed distribution.  This being a *file* as opposed
to a *partition* makes it easier to adjust its size to match your
requirements.  Of course, 2.1GB is way, way, way too small if I were
to hibernate a machine with 64GB of RAM.

That 8.8GB includes a slew of packages in `/mnt/var/cache/apt/archives`
too.  These packages appear to be security and other upgrades applied
just before my order shipped.  Nice bit of paying attention to getting
me an up-to-date pre-installed machine!  Running

``` sh
apt-get clean
```

afterwards would have been nicer still.

The APT sources point to `cn.archive.ubuntu.com`.  That, together with a
default locale of `zh_CN.UTF-8` in `/mnt/etc/default/locale`, clearly
points to a China-centric point of view in preparing my order.  In all
fairness, my order (via Aliexpress) was being prepared by a company
located smack next to Hong Kong and I never indicated that I did not
want Chinese settings to begin with.  But considering that the
shipment was going to Japan (to someone with a very non-Japanese name
to boot), I would have expected either generic or Japanese oriented
settings.

> I realize that users in China can probably only access that server
> but for international orders using `archive.ubuntu.com` is likely to
> lead to a better user experience.

As far as I could determine, there was no software installed outside
the Ubuntu package repositories.  Both `/mnt/user/local` and `/mnt/opt/`
were devoid of files (save a `share/fonts/.uuid`).  There were a few
files below `/mnt/var/lib/snapd/` but I've not used Ubuntu for long
enough now to be familiar with the details of how snaps work.  Based
on file names, they seem to be about integrating with Ubuntu's snap
store.

Checking user accounts in `/mnt/etc/shadow`, I noticed via the `!` in the
`root:!:...` entry that the `root` account has been disabled for login.
This happens by default for Ubuntu installs.  At least it did when I
last installed Ubuntu more than half a decade ago.  The idea is that
you use `sudo` for all administrator related tasks.

The regular user account is `user`.  After asking on the feedback part
of the order site, I was promptly informed that the password for that
account is `123456`.  You probably want to change that to something you
and *only* you know.  From within the Devuan Chimaera Desktop Live

``` sh
sudo passwd --root /mnt user
```

will prompt for a new password and a confirmation.

Checking `root`'s and `user`'s home directories, there were a bunch of
files that are not part of system installation or user account
creation (i.e. the dotfiles in `/mnt/etc/skel/`).  For the `user` account
there were a number of files in a `.cache/` directory, indicating the
account had been used.  There were no `.bash_history` files that could
have shed light on what it was used for.  If I were to hazard a guess,
I'd say applying the package upgrades from a GUI login, maybe even
configure a few settings for use in China.

The installer logs were still available and had a curious timeline in
that some log entries for 2021-12-04 were listed before entries from
2021-12-03.  I'm guessing that was caused by local time versus UTC
settings changing between the various pre-install steps.  Seeing that
those time stamps were just before my order shipped, I guess that the
pre-install is scripted (and might even include manual steps) rather
then using a pre-baked image that gets copied into place.

Anyway, I used `rsync` to create a copy of `/mnt` on my NAS, for backup
and reference purposes, changed the locale default with

``` sh
sed -i 's/zh_CN/C/g' /mtn/etc/default/locale
```

and rebooted into the pre-installed Ubuntu.

# Third and Last Check: Test-Drive Pre-Installed Ubuntu

The login screen is in English now and the `user`'s password is correct
but various bit and pieces of the desktop still use a China-centric
point of view, mostly date/time related.  As I was going to reinstall
from scratch I didn't bother trying to fix that.

I paired my Bluetooth keyboard and mouse without problems (forgot to
note down exactly how, but I'd say via something on (a submenu) of the
`Settings`) and confirmed they worked.

As a last check I started up the browser and played a YouTube video to
test graphics and make sure the monitor's sound worked.  All fine and
dandy!
