---
SPDX-License-Identifier: CC-BY-SA-4.0
SPDX-FileCopyrightText: © 2022 Olaf Meeuwissen
layout: post
title: "Customizing XDG Directory Names"
---

You've just finished installing a new Debian-based Linux system with
your GUI of choice.  You log in for the first time and something
creates a bunch of directories in your `$HOME` directory.  You end up
with

``` console
olaf@basecamp:~$ tree -d -L 1
.
├── Desktop
├── Documents
├── Downloads
├── Music
├── Pictures
├── Public
├── Templates
└── Video

8 directories
```

or something similar and translated into your session's language
(insofar translations are available).

If you log in with another language, these directories are "helpfully"
moved to that language's translated names.  That is, if I were to log
in with my language set to `ja_JP.UTF-8`, my `Desktop/` directory would be
moved to `デスクトップ/`.  Similarly for all the other directories.

I might get asked if I really want that to happen (and would decline
if so), but this move has the obvious potential to break any symbolic
links I might have in other places and throw a monkey wrench into any
scripts where I have not been careful to look up the current value
with `xdg-user-dirs DESKTOP`.  Apart from this "helpful" translation,
I'm not overly happy with the names used in the first place.  Time to
fix that!

# What's Where?

Three files control the behaviour and values of these XDG user
directories.  Their locations are controlled by two environment
variables.  Environment variables first:

- `XDG_CONFIG_DIRS` controls the location of the system-wide
  configuration files.  Its default value is `/etc/xdg/`.
- `XDG_CONFIG_HOME` controls the location of the per-user configuration
  files.  Its default value is `$HOME/.config/`.

So if you want to make changes only for your own account, look for
files below `$XDG_CONFIG_HOME`.  If you want to make changes for *all* of
your users, edit the files below `$XDG_CONFIG_DIRS`.  Don't worry,
per-user settings take precedence so individual users can still
override any system-wide changes you made.

Now the files:

- [`user-dirs.defaults`](https://manpages.debian.org/user-dirs.defaults) specificies the system-wide default names to use
  for the directories.
- [`user-dirs.dirs`](https://manpages.debian.org/user-dirs.dirs) specifies the directory names to use on a per-user
  basis.
- [`user-dirs.conf`](https://manpages.debian.org/user-dirs.conf) controls whether moving directories should be `enabled`
  and which `filename_encoding` to use.  The latter should probably not
  be changed from `UTF-8` now that that encoding has been in widespread
  use for more than a decade.

> You may also find an `$XDG_CONFIG_HOME/user-dirs.locale` that caches
> the value of the locale last used to translate the directory names.

# Customization Examples

Personally, I don't want my directories to be moved around just
because I need to use another language, like for a demo at the office
or something.  To prevent re-translation, I could use

``` config
enabled=False
filename_encoding=UTF-8
```

for my `$XDG_CONFIG_HOME/user-dirs.conf`.

To modify the directory names to taste, I could use something like

``` config
XDG_DOCUMENTS_DIR="$HOME/doc"
XDG_DOWNLOAD_DIR="$HOME/web"
XDG_TEMPLATES_DIR="$HOME/doc/templates"
#XDG_DESKTOP_DIR
#XDG_PUBLICSHARE_DIR
#XDG_MUSIC_DIR
#XDG_PICTURES_DIR
#XDG_VIDEOS_DIR
```

for my `$XDG_CONFIG_HOME/user-dirs.dirs`.  Note that the values need to
resolve to absolute paths.  The commented out values are the only
other documented directory names that are supported.  Also note that
recursive expansion is *not* supported.  If it were, I'd have used
`$XDG_DOCUMENTS_DIR/templates` for that `XDG_TEMPLATES_DIR` value.

> The system-wide `user-dirs.default` should not use absolute paths and
> leave off the `XDG_` prefix and `_DIR` postfix.  Consistency and clarity
> is not exactly a quality exuded by the `xdg-user-dirs` utilities.

# Not Using `xdg-user-dirs`?

I am weaning myself off the desktop, *i.c.* [Xfce](https://xfce.org/), and moving to a setup
where I cobble something together myself.  Doing so, I've noticed that
even if the recommended `xdg-user-dirs` package is not installed there
are some programs that still use the above files to determine where to
put things.

> Seeing that `libglib2.0-0` recommends `xdg-user-dirs` I'd guess that any
> applications depending on that library *might* be doing this.  Then
> again, they might not and use it for other purposes.
