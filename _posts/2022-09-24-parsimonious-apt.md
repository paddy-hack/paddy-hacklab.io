---
SPDX-License-Identifier: CC-BY-SA-4.0
SPDX-FileCopyrightText: © 2022 Olaf Meeuwissen
title: "Parsimonious APT"
layout: post
---

Ever wondered how many packages you have installed?  After you found
out, ever wondered if you really needed all those packages?  And
whether there's a way APT can help you answer that?  If yes, then read
on.

# Taking Stock

To answer that first question, `dpkg-query -W | wc -l` will tell you.  I
ran this on a couple of systems and here are the results:

- 1570 on a freshly, default installed [Debian](https://debian.org) `bookworm`.  The default
  installs the [GNOME](https://www.gnome.org/) desktop.
- 1269 on a freshly, default installed [Devuan](https://devuan.org) `daedalus`.  The default
  installs the [Xfce](https://www.xfce.org/) desktop.
- 1512 on my Devuan `chimaera` laptop.  This machine started out on a
  Devuan's `jessie` default install with an Xfce desktop.  It has been
  upgraded one release at a time over the years and I never really
  bothered much removing things I don't need.
- 595 on my Devuan `daedalus` desktop.  This started from a minimal, as
  in no desktop, Devuan `chimaera` install that was immediately bumped
  to `daedalus`.  I am still piecing things together, one package at a
  time, and am using the [i3](https://i3wm.org/) tiling window manager as a "desktop".

Before we can start answering the remaining questions, we need to know
about two things that the APT package manager takes into account when
figuring out which packages should or should not be installed.

# Package Relations

Every package declares the relations it considers relevant.  The fine
print on [package relations](https://www.debian.org/doc/debian-policy/ch-relationships.html) can be found in the [Debian Policy Manual](https://www.debian.org/doc/debian-policy/)
but for the purposes of this article we only care about

- `Depends` (and `Pre-Depends`), which list the packages that are really
  needed for a package to work as intended
- `Recommends`, which declare what packages are normally expected to be
  installed together with a package
- `Suggests`, which mentions packages that might enhance functionality

The default APT behaviour is to install `Recommends` but leave it up to
you to decide on any `Suggests`.  You can check with

``` sh
apt-config dump | grep APT::Install-
```

what your current settings are.

You can use the `--no-install-recommends` option to `apt` to override on a
one-time basis or add the settings to `/etc/apt/apt.conf` to change the
defaults to your taste.  Using `--install-recommends` can override your
preferences on a one-time basis.

# Package State

In addition to relations, every package installed on your system also
has a state.  The states we care about here are

- `manual` for packages you told APT to install for you
- `auto` for packages that APT installed to satisfy the relations of
  your manually installed packages

# Removing Suggests And Recommends

Putting these two bits of knowledge together, it should be clear that
we can, in principle, remove `auto`matically installed packages that are
not listed as a `Depends` (or `Pre-Depends`).  APT provides an `auto-remove`
command that can be used for this but by default it only considers
packages without *any* relations for removal.  You have to tell it that
you want `auto`matically installed packages with only a `Suggests` as
their strongest relation to all other installed packages to get
removed.  To do so, use

``` sh
apt auto-remove -o APT::AutoRemove::SuggestsImportant=false --assume-no
```

That `--assume-no` will prevent you from accidentally removing packages.
Check the list of packages APT wants to remove and if that looks good,
go ahead and repeat the command without the option.

Similarly for `Recommends`

``` sh
apt auto-remove -o APT::AutoRemove::RecommendsImportant=false --assume-no
```

This will probably show a *long* list of packages and may include some
you'd like to keep.  We'll get to that shortly but those command-lines
are rather long and it's easy to forget the option.  You can add the
following to `/etc/apt/apt.conf` to make your preferences stick.

```
APT::AutoRemove::SuggestsImportant "false";
APT::AutoRemove::RecommendsImportant "false";
```

> If you set the above to `"false"`, you should probably also set the
> corresponding `APT::Install-*` options to `"false"`.  There is next to
> no point in installing, for example, `Recommends` and then have your
> next `apt auto-remove` invocation zap them again.  Even more so if you
> also set `APT::Get::AutomaticRemove` to `"true"`.

A word of warning though, setting the `RecommendsImportant` option to
`"false"` may remove packages *you* actually depend on but your manually
installed packages only list as a `Recommends`.  You will find out,
eventually and the hard way, but figuring out what package(s) you need
to re-install to make things work again is not always straightforward.
You do learn a thing or two that way, though.

# Marking Packages

If APT's `auto-remove` wants to remove something you want to keep, you
can just let it and then reinstall the packages in question but that
is rather inelegant.  Instead, mark the package manually installed.
To mark, for example, `task-xhosa-desktop` that way you'd run

``` sh
apt-mark manual task-xhosa-desktop
```

Conversely, you can also turn a manually installed `task-gnome-desktop`
package into automatically installed with

``` sh
apt-mark auto task-gnome-desktop
```

One reason you might want to mark packages `auto` is because the initial
install leaves a fair number of packages as manually installed.  Many
of these are library packages that are mentioned in a `Depends` relation
by other packages.  This is (probably) due to the way the base system
is installed.  You can peruse the list with

``` sh
apt-mark showmanual
```

For the systems I mentioned at the top, the `manual` package counts are
210, 208, 130 and 74 respectively.  A quick check for package names
starting with `lib` turned up about a hundred packages for the first
two.  Do note that this also matches `libreoffice*` and `lib*-dev` as well
as some packages providing library related tools.

# Going Nuclear

For the adventurous and brave-at-heart, never mind all those well
meant warnings up to here, just go ahead mark *every* package as
automatically installed.  There is a small list of packages the are
considered `Essential` and will not be removed ever (or only when you
try real hard).  Then check what would be removed with

``` sh
apt-mark auto $(dpkg-query -W '${Package}\n')
apt auto-remove --assume-no
```

Cherry-pick that list for things you *know* you want to keep and mark
them `manual`.  You probably want to rinse and repeat, so keep checking
with

``` sh
apt auto-remove --assume-no
```

until satisfied.  Now, there may also be packages you're not sure
about.  You can play it safe and mark them `manual` for now and
re-evaluate that decision later.  This is also a good idea for `task-*`
packages by the way.  The alternative is taking the risk and patch up
any fall-out.  Make sure you make a backup first and keep a rescue
disk handy.

Once satisfied with your "keepers", take the plunge and clean out your
system with

``` sh
apt auto-remove --assume-yes
```

Note, `--assume-yes` will skip asking for confirmation!
