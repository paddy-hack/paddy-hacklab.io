---
SPDX-License-Identifier: CC-BY-SA-4.0
SPDX-FileCopyrightText: © 2022 Olaf Meeuwissen
layout: post
title: "Containerized Jekyll"
---

Many projects have an `Installation` or `Quickstart` section in their
documentation that prompts you to install several bits of software on
your system.  I like to keep my system clean and tidy and installing
random bits of software just to try something out isn't quite my cup
of tea.

To work around my qualms, I use containers whenever possible.  Below
is how I containerized [Jekyll](https://jekyllrb.com).  The results will be used to maintain
these [pages](https://paddy-hack.gitlab.io) in a [GitLab project](https://gitlab.com/paddy-hack/paddy-hack.gitlab.io).

# Picking A Container Image

Jekyll is a [Ruby](https://www.ruby-lang.org/) application so using the [Docker Hub](https://hub.docker.com/)'s [official `ruby`](https://hub.docker.com/_/ruby)
image is pretty much a no-brainer.  That notwithstanding I took a peek
at the `slim` and `alpine` image variants.  These are a *lot* smaller than
the default `latest` image but they lack the tools needed to install
[Ruby Gems](https://rubygems.org/) with native extensions.  So `latest` it will be.

# Quickstart

Noticing in the [Quickstart](https://jekyllrb.com/docs/) section that Jekyll serves pages on port
`4000` by default, I started my journey with

``` sh
cd $HOME/src
mkdir pages
cd pages
docker run --rm -it -v $PWD:/usr/src -w /usr/src \
    -u $(id -u):$(id -g) -p 4000:4000 ruby bash
```

The `docker` invocation runs `bash` inside the `ruby` container.  The
container is started with a number of options that

- mount the current directory *outside* the container on `/usr/src` *inside*
  the container via `-v $PWD:/usr/src`.  This way, its content is
  visible from eiter side.  Note that `-v` needs an absolute path on the
  left-hand side of the `:`, relative paths like `.` don't work.
- change the working directory *inside* the container to `/usr/src` with
  the `-w` option
- change user and group IDs inside the container with the `-u` option.
  The argument fetches my user and group IDs *outside* the container.
  Passing this `-u` option makes sure that files and directories created
  *inside* the container will be owned by me *outside* the container.
  That makes working with such files *outside* the container a lot
  easier.
- expose the container's port to the outside with the `-p` option.  This
  allows for browsing of the Jekyll side from the *outside*.

So far, so good.  Now, on to the [Quickstart](https://jekyllrb.com/docs/) instructions.  It says I
need to

```sh
gem install jekyll bundler
```

Well, the `ruby` container image already includes `bundler` so installing
only `jekyll` should be good enough.  Not installing `bundler` keeps it at
the version that comes with the `ruby` container image.  Here goes

``` console
I have no name!@2068cf1a1011:/usr/src$ gem install jekyll
Fetching terminal-table-2.0.0.gem
Fetching safe_yaml-1.0.5.gem
Fetching rouge-3.28.0.gem
Fetching forwardable-extended-2.6.0.gem
Fetching pathutil-0.16.2.gem
Fetching mercenary-0.4.0.gem
Fetching unicode-display_width-1.8.0.gem
Fetching liquid-4.0.3.gem
Fetching kramdown-2.3.1.gem
Fetching kramdown-parser-gfm-1.1.0.gem
Fetching ffi-1.15.5.gem
Fetching rb-inotify-0.10.1.gem
Fetching rb-fsevent-0.11.1.gem
Fetching listen-3.7.1.gem
Fetching sassc-2.4.0.gem
Fetching jekyll-watch-2.2.1.gem
Fetching em-websocket-0.5.3.gem
Fetching jekyll-sass-converter-2.1.0.gem
Fetching concurrent-ruby-1.1.9.gem
Fetching i18n-1.10.0.gem
Fetching http_parser.rb-0.8.0.gem
Fetching eventmachine-1.2.7.gem
Fetching colorator-1.1.0.gem
Fetching jekyll-4.2.1.gem
Fetching public_suffix-4.0.6.gem
Fetching addressable-2.8.0.gem
Successfully installed unicode-display_width-1.8.0
Successfully installed terminal-table-2.0.0
Successfully installed safe_yaml-1.0.5
Successfully installed rouge-3.28.0
Successfully installed forwardable-extended-2.6.0
Successfully installed pathutil-0.16.2
Successfully installed mercenary-0.4.0
Successfully installed liquid-4.0.3
Successfully installed kramdown-2.3.1
Successfully installed kramdown-parser-gfm-1.1.0
Building native extensions. This could take a while...
Successfully installed ffi-1.15.5
Successfully installed rb-inotify-0.10.1
Successfully installed rb-fsevent-0.11.1
Successfully installed listen-3.7.1
Successfully installed jekyll-watch-2.2.1
Building native extensions. This could take a while...
Successfully installed sassc-2.4.0
Successfully installed jekyll-sass-converter-2.1.0
Successfully installed concurrent-ruby-1.1.9
Successfully installed i18n-1.10.0
Building native extensions. This could take a while...
Successfully installed http_parser.rb-0.8.0
Building native extensions. This could take a while...
Successfully installed eventmachine-1.2.7
Successfully installed em-websocket-0.5.3
Successfully installed colorator-1.1.0
Successfully installed public_suffix-4.0.6
Successfully installed addressable-2.8.0
Successfully installed jekyll-4.2.1
26 gems installed
```

Next on the list is initializing the site

``` console
I have no name!@2068cf1a1011:/usr/src$ jekyll new .
Running bundle install in /usr/src...
  Bundler: Fetching gem metadata from https://rubygems.org/..........
  Bundler: Resolving dependencies...
  Bundler: Using bundler 2.3.3
  Bundler: Using public_suffix 4.0.6
  Bundler: Using colorator 1.1.0
  Bundler: Using concurrent-ruby 1.1.9
  Bundler: Using eventmachine 1.2.7
  Bundler: Using http_parser.rb 0.8.0
  Bundler: Using ffi 1.15.5
  Bundler: Using forwardable-extended 2.6.0
  Bundler: Using rb-fsevent 0.11.1
  Bundler: Using rexml 3.2.5
  Bundler: Using liquid 4.0.3
  Bundler: Using mercenary 0.4.0
  Bundler: Using rouge 3.28.0
  Bundler: Using safe_yaml 1.0.5
  Bundler: Using unicode-display_width 1.8.0
  Bundler: Using kramdown 2.3.1
  Bundler: Using pathutil 0.16.2
  Bundler: Using terminal-table 2.0.0
  Bundler: Using addressable 2.8.0
  Bundler: Using em-websocket 0.5.3
  Bundler: Using i18n 1.10.0
  Bundler: Using kramdown-parser-gfm 1.1.0
  Bundler: Using sassc 2.4.0
  Bundler: Using rb-inotify 0.10.1
  Bundler: Using jekyll-sass-converter 2.1.0
  Bundler: Using listen 3.7.1
  Bundler: Using jekyll-watch 2.2.1
  Bundler: Using jekyll 4.2.1
  Bundler: Fetching jekyll-seo-tag 2.8.0
  Bundler: Fetching jekyll-feed 0.16.0
  Bundler: Installing jekyll-seo-tag 2.8.0
  Bundler: Installing jekyll-feed 0.16.0
  Bundler: Fetching minima 2.5.1
  Bundler: Installing minima 2.5.1
  Bundler: Bundle complete! 6 Gemfile dependencies, 31 gems now installed.
  Bundler: Use `bundle info [gemname]` to see where a bundled gem is installed.`/` is not writable.
  Bundler: Bundler will use `/tmp/bundler20220220-697-lloc57697' as your home directory temporarily.
New jekyll site installed in /usr/src.
```

Just to make sure we record this state, let's put it under `git`'s
version control.  Switching to another terminal

``` console
olaf@basecamp:~/src/pages$ cd $HOME/src/pages
olaf@basecamp:~/src/pages$ git init
Initialized empty Git repository in /home/olaf/src/pages/.git/
olaf@basecamp:~/src/pages$ git add .
olaf@basecamp:~/src/pages$ git commit -m 'feat: Seed blog with result of `jekyll new .`'
[master (root-commit) 9c8e20a] feat: Seed blog with result of `jekyll new .`
 8 files changed, 248 insertions(+)
 create mode 100644 .gitignore
 create mode 100644 404.html
 create mode 100644 Gemfile
 create mode 100644 Gemfile.lock
 create mode 100644 _config.yml
 create mode 100644 _posts/2022-02-20-welcome-to-jekyll.markdown
 create mode 100644 about.markdown
 create mode 100644 index.markdown
```

With the freshly created site's state recorded, switch back to the
terminal that's running the container.  There we'll give running the
server a shot.

``` console
I have no name!@2068cf1a1011:/usr/src$ bundle exec jekyll serve
Configuration file: /usr/src/_config.yml
            Source: /usr/src
       Destination: /usr/src/_site
 Incremental build: disabled. Enable with --incremental
      Generating...
       Jekyll Feed: Generating feed for posts
                    done in 0.165 seconds.
 Auto-regeneration: enabled for '/usr/src'
                    ------------------------------------------------
      Jekyll 4.2.1   Please append `--trace` to the `serve` command
                     for any additional information or backtrace.
                    ------------------------------------------------
/usr/local/bundle/gems/jekyll-4.2.1/lib/jekyll/commands/serve/servlet.rb:3:in `require': cannot load such file -- webrick (LoadError)
	from /usr/local/bundle/gems/jekyll-4.2.1/lib/jekyll/commands/serve/servlet.rb:3:in `<top (required)>'
	from /usr/local/bundle/gems/jekyll-4.2.1/lib/jekyll/commands/serve.rb:179:in `require_relative'
	from /usr/local/bundle/gems/jekyll-4.2.1/lib/jekyll/commands/serve.rb:179:in `setup'
	from /usr/local/bundle/gems/jekyll-4.2.1/lib/jekyll/commands/serve.rb:100:in `process'
	from /usr/local/bundle/gems/jekyll-4.2.1/lib/jekyll/command.rb:91:in `block in process_with_graceful_fail'
	from /usr/local/bundle/gems/jekyll-4.2.1/lib/jekyll/command.rb:91:in `each'
	from /usr/local/bundle/gems/jekyll-4.2.1/lib/jekyll/command.rb:91:in `process_with_graceful_fail'
	from /usr/local/bundle/gems/jekyll-4.2.1/lib/jekyll/commands/serve.rb:86:in `block (2 levels) in init_with_program'
	from /usr/local/bundle/gems/mercenary-0.4.0/lib/mercenary/command.rb:221:in `block in execute'
	from /usr/local/bundle/gems/mercenary-0.4.0/lib/mercenary/command.rb:221:in `each'
	from /usr/local/bundle/gems/mercenary-0.4.0/lib/mercenary/command.rb:221:in `execute'
	from /usr/local/bundle/gems/mercenary-0.4.0/lib/mercenary/program.rb:44:in `go'
	from /usr/local/bundle/gems/mercenary-0.4.0/lib/mercenary.rb:21:in `program'
	from /usr/local/bundle/gems/jekyll-4.2.1/exe/jekyll:15:in `<top (required)>'
	from /usr/local/bundle/bin/jekyll:25:in `load'
	from /usr/local/bundle/bin/jekyll:25:in `<top (required)>'
	from /usr/local/lib/ruby/3.1.0/bundler/cli/exec.rb:58:in `load'
	from /usr/local/lib/ruby/3.1.0/bundler/cli/exec.rb:58:in `kernel_load'
	from /usr/local/lib/ruby/3.1.0/bundler/cli/exec.rb:23:in `run'
	from /usr/local/lib/ruby/3.1.0/bundler/cli.rb:484:in `exec'
	from /usr/local/lib/ruby/3.1.0/bundler/vendor/thor/lib/thor/command.rb:27:in `run'
	from /usr/local/lib/ruby/3.1.0/bundler/vendor/thor/lib/thor/invocation.rb:127:in `invoke_command'
	from /usr/local/lib/ruby/3.1.0/bundler/vendor/thor/lib/thor.rb:392:in `dispatch'
	from /usr/local/lib/ruby/3.1.0/bundler/cli.rb:31:in `dispatch'
	from /usr/local/lib/ruby/3.1.0/bundler/vendor/thor/lib/thor/base.rb:485:in `start'
	from /usr/local/lib/ruby/3.1.0/bundler/cli.rb:25:in `start'
	from /usr/local/lib/ruby/gems/3.1.0/gems/bundler-2.3.3/libexec/bundle:48:in `block in <top (required)>'
	from /usr/local/lib/ruby/3.1.0/bundler/friendly_errors.rb:103:in `with_friendly_errors'
	from /usr/local/lib/ruby/gems/3.1.0/gems/bundler-2.3.3/libexec/bundle:36:in `<top (required)>'
	from /usr/local/bin/bundle:25:in `load'
	from /usr/local/bin/bundle:25:in `<main>'
```

Whoops!  Looks like we hit a [documented issue](https://github.com/github/pages-gem/issues/752).  It's easy enough to fix

``` console
I have no name!@2068cf1a1011:/usr/src$ bundle add webrick
`/` is not writable.
Bundler will use `/tmp/bundler20220220-719-4daecz719' as your home directory temporarily.
Fetching gem metadata from https://rubygems.org/.........
Resolving dependencies...
Bundler will use `/tmp/bundler20220220-719-7f8sq6719' as your home directory temporarily.
Fetching gem metadata from https://rubygems.org/.........
Resolving dependencies...
Using public_suffix 4.0.6
Using bundler 2.3.3
Using concurrent-ruby 1.1.9
Using http_parser.rb 0.8.0
Using ffi 1.15.5
Using rb-fsevent 0.11.1
Using liquid 4.0.3
Using mercenary 0.4.0
Using safe_yaml 1.0.5
Using unicode-display_width 1.8.0
Fetching webrick 1.7.0
Using colorator 1.1.0
Using terminal-table 2.0.0
Using eventmachine 1.2.7
Using forwardable-extended 2.6.0
Using rexml 3.2.5
Using rouge 3.28.0
Using addressable 2.8.0
Using i18n 1.10.0
Using kramdown 2.3.1
Using pathutil 0.16.2
Using kramdown-parser-gfm 1.1.0
Using em-websocket 0.5.3
Using sassc 2.4.0
Using rb-inotify 0.10.1
Using jekyll-sass-converter 2.1.0
Using listen 3.7.1
Using jekyll-watch 2.2.1
Using jekyll 4.2.1
Using jekyll-seo-tag 2.8.0
Using jekyll-feed 0.16.0
Using minima 2.5.1
Installing webrick 1.7.0
```

Let's record this change.  Switching back to that other terminal where
we ran `git` just a minute or so ago

``` console
olaf@basecamp:~/src/pages$ git diff
olaf@basecamp:~/src/pages$ git add .
olaf@basecamp:~/src/pages$ git commit -m 'fix: Add webrick run-time dependency
>
> This gem is no longer bundled with Ruby starting from version 3.0.
>
> See https://github.com/github/pages-gem/issues/752'
[master abef059] fix: Add webrick run-time dependency
 2 files changed, 4 insertions(+)
```

And with that done, it's back to the container terminal where we give
running the server a second shot

``` console
I have no name!@2068cf1a1011:/usr/src$ bundle exec jekyll serve
Configuration file: /usr/src/_config.yml
            Source: /usr/src
       Destination: /usr/src/_site
 Incremental build: disabled. Enable with --incremental
      Generating...
       Jekyll Feed: Generating feed for posts
                    done in 0.155 seconds.
 Auto-regeneration: enabled for '/usr/src'
    Server address: http://127.0.0.1:4000/
  Server running... press ctrl-c to stop.
```

That looks better but meanwhile, on the other terminal

``` console
olaf@basecamp:~/src/pages$ curl --head http://localhost:4000
curl: (56) Recv failure: Connection reset by peer
```

Uh-oh!  What gives?  Well, by default `jekyll serve` binds to `localhost`.
That is, it will only serve requests that originate from within the
container and we just sent a request from the outside.  Switch back to
the container terminal and press `ctrl-c`.

We can make the server bind to the container's IP address with the
`--host` option.  However, figuring out the container's IP address from
the *inside* is not exactly trivial.  Fortunately, the option works fine
if you pass a hostname and `docker` has been kind enough to set that for
us.  It has been staring us in the face in all those container `bash`
prompts.  Better yet, there is a `HOSTNAME` environment variable we can
use too!

> I have since discovered that `hostname -i` returns the IP address
> and that this can be used inside a `ruby:latest` container.

``` console
I have no name!@2068cf1a1011:/usr/src$ bundle exec jekyll serve --host $HOSTNAME
Configuration file: /usr/src/_config.yml
            Source: /usr/src
       Destination: /usr/src/_site
 Incremental build: disabled. Enable with --incremental
      Generating...
       Jekyll Feed: Generating feed for posts
                    done in 0.162 seconds.
 Auto-regeneration: enabled for '/usr/src'
    Server address: http://2068cf1a1011:4000/
  Server running... press ctrl-c to stop.
```

Back in the other terminal we retry our `curl` command

``` console
olaf@basecamp:~/src/pages$ curl --head http://localhost:4000
HTTP/1.1 200 OK
Etag: 9e0853-12a9-6211ecbc
Content-Type: text/html; charset=utf-8
Content-Length: 4777
Last-Modified: Sun, 20 Feb 2022 07:24:44 GMT
Cache-Control: private, max-age=0, proxy-revalidate, no-store, no-cache, must-revalidate
Server: WEBrick/1.7.0 (Ruby/3.1.0/2021-12-25)
Date: Sun, 20 Feb 2022 07:25:42 GMT
Connection: Keep-Alive

```

Success!  Hitting the same URL from the browser also works!

> The container reports a minor cosmetic "ERROR" because it cannot
> find the site's `/favicon.ico` file.  Let's not worry about that now.

Okay, we're done.  Press `ctrl-c` and `exit` the container.

# Preserving Installed Gems

Now, let's start up a container again and serve pages

```console
olaf@basecamp:~/src/pages$ docker run --rm -it -v $PWD:/usr/src -w /usr/src \
    -u $(id -u):$(id -g) -p 4000:4000 ruby bash
I have no name!@d742b01787fb:/usr/src$ bundle exec jekyll serve --host $HOSTNAME
bundler: command not found: jekyll
Install missing gem executables with `bundle install`
```

Where did all the gems we installed go?

Well, they were removed along with our previously exited container
because we told `docker run` to `--rm` the container when done.  The gems
were installed where the `GEM_HOME` environment variable says they
should.  For our `ruby` container image, that's `/usr/local/bundle`.

Unless we install our gems somewhere they persist across containers
getting removed, we will have to run `bundle install` every time we
start a container.  That becomes a bore very fast so let's fix that,
pronto!

We could set `GEM_HOME` inside the current container but let's `exit` and
tell `docker run` to set it for us.  We'll use `/usr/src/vendor/bundle` so
installed gems end up along the source code for our Jekyll site when
we run `bundle install` once inside the container and `exit`.

``` console
olaf@basecamp:~/src/pages$ docker run --rm -it -v $PWD:/usr/src -w /usr/src \
    -u $(id -u):$(id -g) -e GEM_HOME=/usr/src/vendor/bundle -p 4000:4000 ruby bash
I have no name!@1f3dbd3560c0:/usr/src$ bundle install
`/` is not writable.
Bundler will use `/tmp/bundler20220220-7-hh7w2x7' as your home directory temporarily.
Fetching gem metadata from https://rubygems.org/.........
Using bundler 2.3.3
Fetching colorator 1.1.0
Fetching eventmachine 1.2.7
Fetching http_parser.rb 0.8.0
Fetching forwardable-extended 2.6.0
Fetching rb-fsevent 0.11.1
Fetching liquid 4.0.3
Fetching mercenary 0.4.0
Fetching rouge 3.28.0
Fetching unicode-display_width 1.8.0
Fetching webrick 1.7.0
Fetching public_suffix 4.0.6
Fetching concurrent-ruby 1.1.9
Fetching ffi 1.15.5
Using rexml 3.2.5
Fetching safe_yaml 1.0.5
Fetching kramdown 2.3.1
Installing colorator 1.1.0
Installing kramdown 2.3.1
Fetching kramdown-parser-gfm 1.1.0
Installing forwardable-extended 2.6.0
Fetching pathutil 0.16.2
Installing eventmachine 1.2.7 with native extensions
Installing pathutil 0.16.2
Installing liquid 4.0.3
Installing rb-fsevent 0.11.1
Installing http_parser.rb 0.8.0 with native extensions
Installing webrick 1.7.0
Installing safe_yaml 1.0.5
Installing unicode-display_width 1.8.0
Fetching terminal-table 2.0.0
Installing kramdown-parser-gfm 1.1.0
Installing mercenary 0.4.0
Installing ffi 1.15.5 with native extensions
Installing terminal-table 2.0.0
Installing public_suffix 4.0.6
Fetching addressable 2.8.0
Installing rouge 3.28.0
Installing addressable 2.8.0
Installing concurrent-ruby 1.1.9
Fetching i18n 1.10.0
Installing i18n 1.10.0
Fetching em-websocket 0.5.3
Fetching sassc 2.4.0
Fetching rb-inotify 0.10.1
Installing em-websocket 0.5.3
Installing rb-inotify 0.10.1
Fetching listen 3.7.1
Installing sassc 2.4.0 with native extensions
Installing listen 3.7.1
Fetching jekyll-watch 2.2.1
Installing jekyll-watch 2.2.1
Fetching jekyll-sass-converter 2.1.0
Installing jekyll-sass-converter 2.1.0
Fetching jekyll 4.2.1
Installing jekyll 4.2.1
Fetching jekyll-feed 0.16.0
Fetching jekyll-seo-tag 2.8.0
Installing jekyll-seo-tag 2.8.0
Installing jekyll-feed 0.16.0
Fetching minima 2.5.1
Installing minima 2.5.1
Bundle complete! 7 Gemfile dependencies, 32 gems now installed.
Use `bundle info [gemname]` to see where a bundled gem is installed.
I have no name!@1f3dbd3560c0:/usr/src$ exit
exit
```

Checking with `git status` should not show any changes.  Jekyll was kind
enough to initialize a `.gitignore` file that says to ignore `vendor`.

Let's confirm that all the `bundler install`'ed gems have survived the
`exit` above and start a new container

``` console
olaf@basecamp:~/src/pages$ docker run --rm -it -v $PWD:/usr/src -w /usr/src \
    -u $(id -u):$(id -g) -e GEM_HOME=/usr/src/vendor/bundle -p 4000:4000 ruby bash
I have no name!@c1e4c0425202:/usr/src$ bundle install
`/` is not writable.
Bundler will use `/tmp/bundler20220220-7-h13e9x7' as your home directory temporarily.
Using public_suffix 4.0.6
Using addressable 2.8.0
Using bundler 2.3.3
Using colorator 1.1.0
Using concurrent-ruby 1.1.9
Using eventmachine 1.2.7
Using http_parser.rb 0.8.0
Using em-websocket 0.5.3
Using ffi 1.15.5
Using forwardable-extended 2.6.0
Using i18n 1.10.0
Using sassc 2.4.0
Using jekyll-sass-converter 2.1.0
Using rb-fsevent 0.11.1
Using rb-inotify 0.10.1
Using listen 3.7.1
Using jekyll-watch 2.2.1
Using rexml 3.2.5
Using kramdown 2.3.1
Using kramdown-parser-gfm 1.1.0
Using liquid 4.0.3
Using mercenary 0.4.0
Using pathutil 0.16.2
Using rouge 3.28.0
Using safe_yaml 1.0.5
Using unicode-display_width 1.8.0
Using terminal-table 2.0.0
Using jekyll 4.2.1
Using jekyll-feed 0.16.0
Using jekyll-seo-tag 2.8.0
Using minima 2.5.1
Using webrick 1.7.0
Bundle complete! 7 Gemfile dependencies, 32 gems now installed.
Use `bundle info [gemname]` to see where a bundled gem is installed.
```

Good!  It's using all the gems we have already installed.

Note that `bundler` issues a minor warning

``` console
`/` is not writable.
Bundler will use `/tmp/bundler20220220-7-h13e9x7' as your home directory temporarily.
```

That comes about because we insist on setting the user (and group) IDs
via `docker`'s `-u` option.  If those IDs are not known inside the
container, the `HOME` environment variable will be set to `/`.  We can
fix that by setting the `HOME` environment variable explicitly on the
`docker` command line.  All that's needed is setting the variable with
`-e HOME=/usr/src`.  After doing so, `bundler` will start maintaining a
cache below `$HOME/.bundle/cache/`.  There is no point in version
controlling caches so

``` sh
echo '/.bundle/cache/' >> .gitignore
git add .
git commit -m 'fix: Keep /.bundle/cache/ out of the repository'
```

Looking closer, you may now also see a `.bash_history` appear in your
`git status` output.  Before we set `HOME` to coincide with the location
of our source outside the container, it ended up in `/.bash_history` so
we never noticed.  Stuffing a list of the commands you run inside the
container in a version control system is pretty pointless, so

``` console
olaf@basecamp:~/src/pages$ echo '/.bash_history' >> .gitignore
olaf@basecamp:~/src/pages$ git add .gitignore
olaf@basecamp:~/src/pages$ git commit -m 'fix: Keep shell history out of the repository'
[master 54165df] fix: Keep shell history out of the repository
 1 file changed, 1 insertion(+)
```

## Shorter Command Lines

Those `docker` command lines are awfully long.  There's no way I'm going
to remember what options are needed for this project a week from now.
I'm not even sure I'll be able to recall them tomorrow.

Let's address that and stuff them in a shell script.  Inspired by
[Scripts To Rule Them All](https://github.com/github/scripts-to-rule-them-all#readme), let's create a wrapper script to start a
"console", i.e. get a command line inside the container, and another
one to start the server.  The latter builds on the former.

You can find their latest incarnations in the [`git` repository](https://gitlab.com/paddy-hack/paddy-hack.gitlab.io/-/tree/main) but as of
writing they look like this

``` console
olaf@basecamp:~/src/pages$ cat scripts/console
#!/bin/sh -eu
# SPDX-License-Identifier: CC-BY-SA-4.0
# SPDX-FileCopyrightText: © 2022 Olaf Meeuwissen

docker run --rm -it \
       -v $PWD:/usr/src \
       -w /usr/src \
       -u $(id -u):$(id -g) \
       -e HOME=/usr/src \
       -e GEM_HOME=/usr/src/vendor/bundle \
       -p 4000:4000 \
       ruby \
       bash "$@"
```

``` console
olaf@basecamp:~/src/pages$ cat scripts/server
#!/bin/sh -eu
# SPDX-License-Identifier: CC-BY-SA-4.0
# SPDX-FileCopyrightText: © 2022 Olaf Meeuwissen

scripts/console -c 'bundle exec jekyll serve --host $HOSTNAME'
```

Now it also becomes clearer why I insisted on setting environment
variables on the `docker` command line rather than inside the container.
Doing the latter would lead to a mighty ugly `bash` invocation.
Something along the lines of

``` sh
bash -c "export HOME=/usr/src; \
         export GEM_HOME=/usr/src/vendor/bundle; \
         exec bash \"$@\""
```

where the `exec bash` bit replaces the `bash -c` process so we don't
waste a process by running `bash` inside of `bash`.

That's all!
