---
SPDX-License-Identifier: CC-BY-SA-4.0
SPDX-FileCopyrightText: © 2022 Olaf Meeuwissen
layout: post
title: "Running Containerized Rails System Tests"
---

Having [containerized Rails]({% link _posts/2022-02-27-containerized-rails.md %}), I ran into a little snag trying to run its
system tests.  This comes about because `bundle exec rails new` assumes
you are doing native development.  It sets up system test support
assuming you have `chrome` (or `chromium`) available and that the browser
can display the pages that are accessed when running the system tests.
Neither assumption is true in a containerized setup so I had some work
to do.

I spent the better part of two days reading blog posts, documentation
pages and trying out things to arrive at a solution I was happy with.
In the course of that, I also came across a few alternative solutions
and thought it was a waste to not record those somewhere.  So, hence
this post.

# Goal

What I wanted to achieve is being able to run the system tests from my
app container against a browser that runs in a "sidecar" container.
This will involve working with multiple containers so I resorted to
`docker-compose` to do the heavy lifting of configuring containers and
setting up their networking so they can communicate.

# Preparations

The `docker-compose.yaml` file defines an `app` service as follows

``` yaml
services:
  app:
    environment:
      BUNDLE_APP_CONFIG: /usr/src/.bundle
      HOME: /usr/src
    image: ruby:latest
    user: 1000:1000
    volumes:
      - .:/usr/src
    working_dir: /usr/src
```

You may want to adjust the `user` attribute to match your user and
primary group IDs.  Use `id -u` and `id -g`, respectively, to find out
what they are.

I started with a `Gemfile` that looked like

``` ruby
source "https://rubygems.org"
ruby ">= 2.7.0"                 # per Rails 7.0 documentation

gem "rails", "~> 7.0"
```

Next, I seeded a new Rails application and generated a scaffold for a
trivial `book` model.

``` sh
docker-compose run --rm app bash -c 'bundle config set path vendor/gems'
docker-compose run --rm app bash -c 'bundle install'
docker-compose run --rm app bash -c 'bundle exec rails new --force .'
docker-compose run --rm app bash -c 'bin/rails generate scaffold
    book author:string title:string'
docker-compose run --rm app bash -c 'bin/setup'
```

## The Situation

Let's take a quick peek at the bits of code that the above put in
place that are relevant to system tests.  First, from the `Gemfile`

``` ruby
group :test do
  gem "capybara"
  gem "selenium-webdriver"
  gem "webdrivers"
end
```

and in `test/application_system_test_case.rb`

``` ruby
require "test_helper"

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  driven_by :selenium, using: :chrome, screen_size: [1400, 1400]
end
```

## The Symptoms

Now that we have taken care of the preparations and gotten our
bearings, let's see what running the system tests produces.

``` console
olaf@basecamp:~$ docker-compose run --rm app bash -c 'bin/rails test:system'
Creating system-test_app_run ... done
/usr/src/vendor/gems/ruby/3.1.0/gems/webdrivers-5.0.0/lib/webdrivers/chrome_finder.rb:21:in `location': Failed to find Chrome binary. (Webdrivers::BrowserNotFound)
	from /usr/src/vendor/gems/ruby/3.1.0/gems/webdrivers-5.0.0/lib/webdrivers/chrome_finder.rb:10:in `version'
[...]
```

The `webdrivers` gem complains it cannot find the `chrome` binary.  I have
already mentioned that that is something `rails new` assumes to be
available so no surprise there.  Let's see what we can do about this.

# Running A Browser In The App Container

We can start an interactive container with `root` privileges and install
`chromium` to see if that solves things.

``` console
olaf@basecamp:~$ docker-compose run --rm -u 0 app bash
root@a91977a209fb:~# apt update
Get:1 http://security.debian.org/debian-security bullseye-security InRelease [44.1 kB]
[...]
root@a91977a209fb:~# apt install --no-install-recommends --assume-yes chromium
Get:1 http://deb.debian.org/debian bullseye/main amd64 libapparmor1 amd64 2.13.6-10 [99.3 kB]
[...]
root@a91977a209fb:~# bin/rails test:system
Running 4 tests in a single process (parallelization threshold is 50)
Run options: --seed 34648

# Running:

DEBUGGER: Attaching after process 1620 fork to child process 1702
DEBUGGER[bin/rails#1729]: Attaching after process 1620 fork to child process 1729
E

Error:
BooksTest#test_visiting_the_index:
Selenium::WebDriver::Error::UnknownError: unknown error: Chrome failed to start: exited abnormally.
  (unknown error: DevToolsActivePort file doesn't exist)
  (The process started from chrome location /usr/bin/chromium is no longer running, so ChromeDriver is assuming that Chrome has crashed.)
    test/system/books_test.rb:9:in `block in <class:BooksTest>'
    [...]

Finished in 2.599239s, 1.5389 runs/s, 0.0000 assertions/s.
4 runs, 0 assertions, 0 failures, 4 errors, 0 skips
^Crails aborted!
[...]
```

Great, not!

> BTW, I hit `Ctrl-C` to get the command prompt back.

Chrome fails to start with a frustratingly cryptic error message.  I
eventually found out that you need to run Chrome in headless mode *and*
set the `--no-sandbox` option in `test/application_system_test_case.rb`
for things to work as expected.  Just using one or the other by itself
in *not* good enough.  Editing that file to look like

``` ruby
require "test_helper"

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  driven_by :selenium, using: :headless_chrome do |option|
    option.add_argument("no-sandbox")
  end
end
```

where I haven't bothered with the `screen_size` option anymore, we get

``` console
root@a91977a209fb:~# bin/rails test:system
Running 4 tests in a single process (parallelization threshold is 50)
Run options: --seed 55813

# Running:

DEBUGGER: Attaching after process 2637 fork to child process 2670
Capybara starting Puma...
* Version 5.6.4 , codename: Birdie's Version
* Min threads: 0, max threads: 4
* Listening on http://127.0.0.1:37293
....

Finished in 1.903072s, 2.1019 runs/s, 2.1019 assertions/s.
4 runs, 4 assertions, 0 failures, 0 errors, 0 skips
```

Success!  Not quite what I am after but at least the system tests run
successfully.

> I actually found it easier to get this to work using Firefox.  Just
> install `firefox-esr` and modify `ApplicationSystemTestCase` to read
>
> ``` ruby
> class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
>   driven_by :selenium, using: :headless_firefox
> end
> ```
> This requires running the tests as a non-`root` user.  Use
>
> ``` sh
> container=$(docker ps | grep $(basename $PWD)_app_run | cut -d' ' -f1)
> docker exec -it -u $(id -u):$(id -g) $container bash
> ```
> to enter the running container through the backdoor if you want to
> give this a try.

## Using An Alternative WebDriver

We have been using `selenium` to drive the browser during the system
tests above.  Entering the scene well after `selenium`, there is now an
alternative called [`cuprite`](https://github.com/rubycdp/cuprite).  To use that, add the `cuprite` gem to the
`Gemfile`

``` ruby
group :test do
  gem "capybara"
  gem "cuprite"
end
```

> Yup!  You don't need the `webdrivers` and `selenium-webdriver` gems.
> There's no need to remove them from `vendor/gems/`.  Running via
> `bin/rails` will only load what's in the `Gemfile`.

Next, rewrite `test/application_system_test_case.rb` to read

``` ruby
require "test_helper"
require "capybara/cuprite"

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  driven_by :cuprite, options: { browser_options: { "no-sandbox": nil } }
end
```

Finally, running `bin/rails test:system` should succeed.

This works fine with Chrome installed in the container.  I have not
been able to make it work with Firefox but did not try very hard.
Simply adding `browser_name: :firefox` to the `options` did not do the
trick.

# Running A Browser On The Host

If you're still in the interactive container as `root`, `exit` it and let
`docker-compose` clean it up for you, courtesy of the `--rm` option.  If
you modified the `Gemfile` to try `cuprite`, change it back to

``` ruby
group :test do
  gem "capybara"
  gem "selenium-webdriver"
end
```

That's right, no `webdrivers` gem anymore.  It probes for a browser
application in the current run-time environment, the container in our
case, but we want to keep that browser out of the container.  The
`selenium-webdriver` does not insist on the presence of a browser and we
will set it up to talk to a `chromedriver` instance running on the host.

> Run `apt install --no-install-recommends --assume-yes chromedriver` on
> the host if you don't have `chromedriver` installed already.  Adjust
> as required for non-APT systems.

In a dedicated terminal, run

``` sh
chromedriver --allowed-ips=
```

This is *not* secure but we will ignore that little detail for this
test.

> I run a deny-all firewall setup on my machine, using `nftables`, so
> things aren't wide open anyway.  For this test, I had to allow all
> network traffic on the `br-*` device that Docker creates.  That looks
> like
>
> ``` sh
> nft add rule inet filter input iifname 'br-*' accept
> ```
> and needs to be run as `root`.  Feel free to narrow that down to the
> exact interface name if so inclined.

Rewrite `test/application_system_test_case.rb` to resemble

``` ruby
require "test_helper"

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  Capybara.server_host = `hostname -i`.strip
  driven_by :selenium, using: :chrome, options: {
              browser: :remote,
              url: "http://192.168.11.8:9515"
            }
end
```

Make sure to replace the IP address in the `url` with that of your own
host.  The `9515` port number is the default port used by `chromedriver`.

With all that in place, the system tests should succeed.

``` console
olaf@basecamp:~/tmp/system-test$ docker-compose run --rm app bash -c 'bin/rails test:system'
Creating system-test_app_run ... done
Running 4 tests in a single process (parallelization threshold is 50)
Run options: --seed 48881

# Running:

Capybara starting Puma...
* Version 5.6.4 , codename: Birdie's Version
* Min threads: 0, max threads: 4
* Listening on http://172.22.0.2:38921
....

Finished in 1.856486s, 2.1546 runs/s, 2.1546 assertions/s.
4 runs, 4 assertions, 0 failures, 0 errors, 0 skips
```

Good, we've managed to keep the browser out of the `app` container!
However, we still need to arrange for `chromedriver` to be installed on
the host.  Time to fix that and run a browser in a "sidecar"
container.

> BTW, I gave the above a quick try with `cuprite` (using only the `url`
> option) but without any success.  Also, this approach cannot be used
> with Firefox.

# Running A Browser In A Sidecar Container

Go ahead, terminate that `chromedriver` process with `Ctrl-C` and maybe
even purge the package from your system.  We're going to run a browser
in a sidecar container, a container that runs right next to the `app`
container.  We'll be using two different browser images, one for each
of the WebDriver gems.  Why?  Well, I have not figured out how to use
the same image for both (yet?), that's why ;-)

Let's add services to `docker-compose.yaml`, like so

``` yaml
  standalone-chrome:
    image: selenium/standalone-chrome

  browserless:
    image: browserless/chrome
```

> You probably should add versions to the images for reliable test
> results during development.  If you do, make sure to occasionally
> upgrade the version when your system tests are green.  That way,
> your test results stay relevant for current Chrome versions.

Let's look at using the `selenium` webdriver first.  Start a browser in
the foreground (so we can see what it's up to) with

``` sh
docker-compose up standalone-chrome
```

Next, edit `test/application_system_test_case.rb` to

``` ruby
require "test_helper"

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  Capybara.server_host = `hostname`.strip
  driven_by :selenium, using: :chrome, options: {
              browser: :remote,
              url: "http://standalone-chrome:4444/wd/hub"
            }
end
```

The `url` can use the service name as the hostname and the default port
number to connect to the WebDriver Hub.

Running the system tests should succeed again

``` console
olaf@basecamp:~/tmp/system-test$ docker-compose run --rm app bash -c 'bin/rails test:system'
Creating system-test_app_run ... done
Running 4 tests in a single process (parallelization threshold is 50)
Run options: --seed 40385

# Running:

Capybara starting Puma...
* Version 5.6.4 , codename: Birdie's Version
* Min threads: 0, max threads: 4
* Listening on http://172.22.0.3:42551
....

Finished in 1.902307s, 2.1027 runs/s, 2.1027 assertions/s.
4 runs, 4 assertions, 0 failures, 0 errors, 0 skips
```

> If you expose the `standalone-chrome` service's internal `7900` port,
> you can see the tests in action in your browser.  Point it at the
> external port number you have configured on `localhost`, connect to
> `noVNC` (password is `secret`), run the system tests and watch in awe.

For `cuprite`, start the `browserless` service, add the gem to the `Gemfile`
and update installed gems if necessary with `bin/setup`.

``` sh
docker-compose up browserless		# in a dedicated terminal
sed -i 's/selenium-webdriver/cuprite/' Gemfile
docker-compose run --rm app bash -c 'bin/setup'
```

Edit the `test/application/system_test_case.rb` file to read

``` ruby
require "test_helper"
require "capybara/cuprite"

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  Capybara.server_host = `hostname`.strip
  driven_by :cuprite, options: { url: "http://browserless:3000" }
end
```

and the system tests should still be all green.

# Conclusion

That's it.  For the purposes of this post, I have tried to keep the
focus on getting simple scaffold system tests to succeed without the
need to install a browser in my `app` container (or a browser driver on
my host OS for that matter).  The preceding section shows two methods
to achieve this using only "off the shelf" containers.

For my own app development, I'm going with the `cuprite` webdriver and a
sidecar container running `browserless/chrome`.  I still need to check
up on additional options that I may want to add to the `browserless`
service and `ApplicationSystemTestCase` to accommodate more advanced
system test usage but for now I'm good.

# References

There were three blog posts that helped me piece things together.
They include advice that is no longer needed, target more advanced
usage or cover additional niceties.  Just in case I want to refer to
them later (in no particular order):

- [Rails System Tests In Docker](https://hint.io/blog/rails-system-test-docker)
- [RUN RAILS 6 SYSTEM TESTS IN DOCKER USING A HOST BROWSER](https://avdi.codes/run-rails-6-system-tests-in-docker-using-a-host-browser/)
- [System of a test: Proper browser testing in Ruby on Rails](https://evilmartians.com/chronicles/system-of-a-test-setting-up-end-to-end-rails-testing)
