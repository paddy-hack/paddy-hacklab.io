---
SPDX-License-Identifier: CC-BY-SA-4.0
SPDX-FileCopyrightText: © 2022 Olaf Meeuwissen
title: "Publishing GitLab Pages With Jekyll"
layout: post
---

It's all nice and dandy to containerize Jekyll and [write up]({% link _posts/2022-02-20-containerized-jekyll.md %}) how you
went about doing so but wouldn't it be nice if the whole world could
read it too?

Enter [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

There is a [Jekyll Pages template project](https://gitlab.com/pages/jekyll) that I took a peek at, but I
decided to start from scratch and created a [blank project](https://gitlab.com/projects/new#blank_project).  The bits
that I filled out on that page were

- `Project name`: `paddy-hack.gitlab.io`.  Use your own GitLab username.
  The `Project slug` will default to the same.
- `Project deployment target`: `GitLab Pages`.  I'm not sure if this has
  any effect but this is the target I want to deploy to so I thought I
  might as well set it.  It's optional, by the way.
- `Visibility level`: `Public`.  Hey!  I want the world to know, right?
  Using `Private` here will require people to be a member of the project
  and log in before they can browse the pages.
- unchecked the box to `Initialize repository with a README` because I
  already have a repository I want to push.

With that done, I hit the `Create project` button.  Greeted by the
project's top page, I went ahead and configured my `~/src/pages`
repository to use GitLab as its `origin`.

``` sh
cd ~/src/pages
git remote add origin git@gitlab.com:paddy-hack/paddy-hack.gitlab.io.git
git push -u origin main
```

Note that my repository had no `remote` named `origin` yet.  Furthermore,
only the `main` branch was ready for pushing and there were no tags yet.

Based on what I had gleaned from the [Jekyll Pages template project](https://gitlab.com/pages/jekyll), I
figured that for now this minimal `.gitlab-ci.yaml`, with an `a` in `yaml`!,
would do.

``` yaml
pages:
  image: ruby
  script:
    - bundle install
    - bundle exec jekyll build -d public
  artifacts:
    paths:
      - public
```

Committed and pushed.  No CI/CD pipeline started.

Doh, GitLab only looks for `.gitlab-ci.yml`, without an `a` in `yaml`, by
default.  I changed the file to look at in the `Settings` >> `CI/CD` >>
`General pipelines` >> `CI/CD configuration file` but the `CI/CD` >>
`Pipelines` menu stubbornly thought my project still didn't have a CI/CD
configuration file.  The `CI/CD` >> `Editor` disagreed and showed the file
I had pushed.  The `CI/CD` >> `Jobs` menu also wanted me to create a CI/CD
configuration file.

> There is an [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/26169) that wants to get the above fixed.  I [added](https://gitlab.com/gitlab-org/gitlab/-/issues/26169#note_851888515) my two
> yen.

While poking around [my pages project](https://gitlab.com/paddy-hack/paddy-hack.gitlab.io), I also noticed that the top page
displayed the `index.markdown` file Jekyll had created.  That's a pretty
lame "default" introduction for a project, so I put an almost equally
lame stub `README.md` in place and pushed it, expecting the CI/CD
pipeline to trigger.

``` console
olaf@basecamp:~/src/pages$ git add README.md
olaf@basecamp:~/src/pages$ git commit
[main 2c6281b] feat: Replace project top page with README.md
 1 file changed, 18 insertions(+)
 create mode 100644 README.md
olaf@basecamp:~/src/pages$ git push
Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 16 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 1.35 KiB | 1.35 MiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
To gitlab.com:paddy-hack/paddy-hack.gitlab.io.git
   f885a04..2c6281b  main -> main
```

The [CI/CD pipeline](https://gitlab.com/paddy-hack/paddy-hack.gitlab.io/-/pipelines/476694738) triggered, the [job](https://gitlab.com/paddy-hack/paddy-hack.gitlab.io/-/jobs/2122363485) completed and the pages were
published/deployed at `https://paddy-hack.gitlab.io`.  The published
site is also available as a job [artifact](https://gitlab.com/paddy-hack/paddy-hack.gitlab.io/-/jobs/2122363485/artifacts/download).  Note that artifacts will be
deleted after they expire by default.  Only the last artifact (on a
branch) will be kept.  You can tell GitLab to `Keep` artifacts on the
job's page.  I did that for this first job so I can link to it from
this post.

Looking through the job artifact, here's what it contains

``` console
olaf@basecamp:~/src/pages$ tree -a public/
public/
├── 2022
│   └── 02
│       └── 20
│           └── containerized-jekyll.html
├── 404.html
├── README.html
├── about
│   └── index.html
├── assets
│   ├── main.css
│   ├── main.css.map
│   └── minima-social-icons.svg
├── feed.xml
├── index.html
├── jekyll
│   └── update
│       └── 2022
│           └── 02
│               └── 20
│                   └── welcome-to-jekyll.html
└── scripts
    ├── console
    └── server

11 directories, 12 files
```

That could use some cleaning up.  For starters, the "Welcome to
Jekyll" post was put there by `jekyll new` and isn't really part of my
pages collection.  Although you need to enter the URL yourself to
access it, the `README.html`, generated from `README.md`, was not really
meant to be published.  It is just a project administrative kind of
file.  The "About" page (`about/index.html`) needs a rewrite so it
applies to my collection of pages.  It is generated from
`about.markdown`.  Hmm, I prefer `.md` extensions so that needs some work
too.  Finally, I'm not sure I want to publish the `scripts`.  They look
a lot better in the GitLab UI for the [project's repository](https://gitlab.com/paddy-hack/paddy-hack.gitlab.io/-/tree/main).

Looking at the rendered pages, there is a bunch of meta-data that
needs tweaking too, so I'm off now, fixing things up.
