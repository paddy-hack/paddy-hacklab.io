---
SPDX-License-Identifier: CC-BY-SA-4.0
SPDX-FileCopyrightText: © 2022 Olaf Meeuwissen
layout: post
title: "Containerized Rails"
---

This post builds on [Containerized Jekyll]({% link _posts/2022-02-20-containerized-jekyll.md %}).  Since [Rails](https://rubyonrails.org) is implemented
in [Ruby](https://www.ruby-lang.org/) just like [Jekyll](https://jekyllrb.com), a lot of that post applies with only minor
modifications.

# Getting Started

Using the `ruby` container image, I started working my way through the
[Getting Started](https://guides.rubyonrails.org/getting_started.html) guide.  The container is started with

``` sh
docker run --rm -it -v $PWD:/usr/src -w /usr/src \
    -u $(id -u) -p 3000:3000 ruby bash
```

where I've taken care to use my own user ID while in the container.
The default port for Rails applications, `3000`, is exposed so we can
access the application at `http://localhost:3000/` from outside of the
container.  The current working directory is made available inside the
container at an, hopefully, intuitive location.  After entering the
container, we are put in that directory at a shell prompt.

The default command for the container is `irb`, the interactive `ruby`
interpreter, but we want a regular command line shell.

The environment inside the container looks like this

``` console
I have no name!@af1a395d08a9:/usr/src$ export
declare -x BUNDLE_APP_CONFIG="/usr/local/bundle"
declare -x BUNDLE_SILENCE_ROOT_WARNING="1"
declare -x GEM_HOME="/usr/local/bundle"
declare -x HOME="/"
declare -x HOSTNAME="200a55987dec"
declare -x LANG="C.UTF-8"
declare -x OLDPWD
declare -x PATH="/usr/local/bundle/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
declare -x PWD="/usr/src"
declare -x RUBY_DOWNLOAD_SHA256="1a0e0b69b9b062b6299ff1f6c6d77b66aff3995f63d1d8b8771e7a113ec472e2"
declare -x RUBY_MAJOR="3.1"
declare -x RUBY_VERSION="3.1.0"
declare -x SHLVL="1"
declare -x TERM="xterm"
I have no name!@af1a395d08a9:/usr/src$ echo $UID
1000
```

Note that the `HOSTNAME` is generated by `docker` and will vary from run
to run.  The `PWD` value is what we passed to `docker`'s `-w` option.  `UID`
is a `bash` variable, technically not part of the environment, so we
have to inspect it via `echo` or similar.

It's good to see that the `LANG` value is set to support UTF-8.  Take
note of the `GEM_HOME` variable which points to a locationq *inside* the
container.  So does `BUNDLE_APP_CONFIG`.  This means that when you exit
the container, all the gems that you install will disappear.  We will
want to do something about that but that's for later.

# Alternative Container Images

The `ruby` image is Debian based, installs recommended packages and
does not make much of an effort to stay small.  Two leaner, meaner
alternatives are `ruby:slim`, also Debian based, and `ruby:alpine`.
The latter doesn't come with `bash` so you have to use its `busybox`
based `sh` instead.

Both images do not have the support needed to install Ruby gems with
native extensions, e.g. `websocket-driver`, so let's stick with `ruby`
(a.k.a. `ruby:latest`) for now.

# Creating a New Rails Project

Let's check the "prerequisites" listed in the [Getting Started](https://guides.rubyonrails.org/getting_started.html) guide.

``` console
I have no name!@af1a395d08a9:/usr/src$ ruby --version
ruby 3.1.0p0 (2021-12-25 revision fb4df44d16) [x86_64-linux]
I have no name!@af1a395d08a9:/usr/src$ sqlite3 --version
bash: sqlite3: command not found
I have no name!@af1a395d08a9:/usr/src$ node --version
bash: node: command not found
I have no name!@af1a395d08a9:/usr/src$ yarn --version
bash: yarn: command not found
I have no name!@af1a395d08a9:/usr/src$ yarnpkg --version
bash: yarnpkg: command not found
```

> Note that Debian has renamed `yarn` to `yarnpkg` so as to not conflict
> with the `yarn` command from its `cmdtest` package.

The above doesn't look very encouraging with only the `ruby` command
available but let's proceed anyway.  We can always add things when we
find out we really need them.

``` console
I have no name!@af1a395d08a9:/usr/src$ gem install rails
Fetching tzinfo-2.0.4.gem
Fetching method_source-1.0.0.gem
Fetching i18n-1.10.0.gem
Fetching thor-1.2.1.gem
Fetching zeitwerk-2.5.4.gem
Fetching concurrent-ruby-1.1.9.gem
Fetching activesupport-7.0.2.2.gem
Fetching nokogiri-1.13.3-x86_64-linux.gem
Fetching crass-1.0.6.gem
Fetching loofah-2.14.0.gem
Fetching rails-html-sanitizer-1.4.2.gem
Fetching rails-dom-testing-2.0.3.gem
Fetching rack-2.2.3.gem
Fetching rack-test-1.1.0.gem
Fetching erubi-1.10.0.gem
Fetching builder-3.2.4.gem
Fetching actionview-7.0.2.2.gem
Fetching actionpack-7.0.2.2.gem
Fetching railties-7.0.2.2.gem
Fetching mini_mime-1.1.2.gem
Fetching marcel-1.0.2.gem
Fetching activemodel-7.0.2.2.gem
Fetching activerecord-7.0.2.2.gem
Fetching globalid-1.0.0.gem
Fetching activejob-7.0.2.2.gem
Fetching activestorage-7.0.2.2.gem
Fetching actiontext-7.0.2.2.gem
Fetching mail-2.7.1.gem
Fetching actionmailer-7.0.2.2.gem
Fetching actionmailbox-7.0.2.2.gem
Fetching websocket-extensions-0.1.5.gem
Fetching websocket-driver-0.7.5.gem
Fetching rails-7.0.2.2.gem
Fetching nio4r-2.5.8.gem
Fetching actioncable-7.0.2.2.gem
Successfully installed zeitwerk-2.5.4
Successfully installed thor-1.2.1
Successfully installed method_source-1.0.0
Successfully installed concurrent-ruby-1.1.9
Successfully installed tzinfo-2.0.4
Successfully installed i18n-1.10.0
Successfully installed activesupport-7.0.2.2
Successfully installed nokogiri-1.13.3-x86_64-linux
Successfully installed crass-1.0.6
Successfully installed loofah-2.14.0
Successfully installed rails-html-sanitizer-1.4.2
Successfully installed rails-dom-testing-2.0.3
Successfully installed rack-2.2.3
Successfully installed rack-test-1.1.0
Successfully installed erubi-1.10.0
Successfully installed builder-3.2.4
Successfully installed actionview-7.0.2.2
Successfully installed actionpack-7.0.2.2
Successfully installed railties-7.0.2.2
Successfully installed mini_mime-1.1.2
Successfully installed marcel-1.0.2
Successfully installed activemodel-7.0.2.2
Successfully installed activerecord-7.0.2.2
Successfully installed globalid-1.0.0
Successfully installed activejob-7.0.2.2
Successfully installed activestorage-7.0.2.2
Successfully installed actiontext-7.0.2.2
Successfully installed mail-2.7.1
Successfully installed actionmailer-7.0.2.2
Successfully installed actionmailbox-7.0.2.2
Successfully installed websocket-extensions-0.1.5
Building native extensions. This could take a while...
Successfully installed websocket-driver-0.7.5
Building native extensions. This could take a while...
Successfully installed nio4r-2.5.8
Successfully installed actioncable-7.0.2.2
Successfully installed rails-7.0.2.2
35 gems installed
I have no name!@af1a395d08a9:/usr/src$ rails --version
Rails 7.0.2.2
```

As I ran this under my own user ID, I was slightly surprised to see
the installation succeed.  I had expected permission issues to prevent
me from installing below `GEM_HOME`.  However

``` console
I have no name!@af1a395d08a9:/usr/src$ groups
root
I have no name!@af1a395d08a9:/usr/src$ ls -dl $GEM_HOME
drwxrwxrwx 1 root root 4096 Feb 27 04:01 /usr/local/bundle
```

quickly explained why that didn't happen.  Inside the container, I'm
part of the `root` group which has read/write permissions on that
location.  Actually, *everyone* has read/write permissions there.  This
means I can also pass my `GID` to `docker` with `-u $(id -u):$(id -g)` and
avoid all permission issues outside the container!

Time to create the Getting Started guide's blog application but let's
do so in the current working directory instead of a `blog` directory.

``` console
I have no name!@af1a395d08a9:/usr/src$ rails new .
       exist
      create  README.md
      create  Rakefile
      create  .ruby-version
      create  config.ru
      create  .gitignore
      create  .gitattributes
      create  Gemfile
         run  git init from "."
hint: Using 'master' as the name for the initial branch. This default branch name
hint: is subject to change. To configure the initial branch name to use in all
hint: of your new repositories, which will suppress this warning, call:
hint:
hint: 	git config --global init.defaultBranch <name>
hint:
hint: Names commonly chosen instead of 'master' are 'main', 'trunk' and
hint: 'development'. The just-created branch can be renamed via this command:
hint:
hint: 	git branch -m <name>
Initialized empty Git repository in /usr/src/.git/
      create  app
      create  app/assets/config/manifest.js
      create  app/assets/stylesheets/application.css
      create  app/channels/application_cable/channel.rb
      create  app/channels/application_cable/connection.rb
      create  app/controllers/application_controller.rb
      create  app/helpers/application_helper.rb
      create  app/jobs/application_job.rb
      create  app/mailers/application_mailer.rb
      create  app/models/application_record.rb
      create  app/views/layouts/application.html.erb
      create  app/views/layouts/mailer.html.erb
      create  app/views/layouts/mailer.text.erb
      create  app/assets/images
      create  app/assets/images/.keep
      create  app/controllers/concerns/.keep
      create  app/models/concerns/.keep
      create  bin
      create  bin/rails
      create  bin/rake
      create  bin/setup
      create  config
      create  config/routes.rb
      create  config/application.rb
      create  config/environment.rb
      create  config/cable.yml
      create  config/puma.rb
      create  config/storage.yml
      create  config/environments
      create  config/environments/development.rb
      create  config/environments/production.rb
      create  config/environments/test.rb
      create  config/initializers
      create  config/initializers/assets.rb
      create  config/initializers/content_security_policy.rb
      create  config/initializers/cors.rb
      create  config/initializers/filter_parameter_logging.rb
      create  config/initializers/inflections.rb
      create  config/initializers/new_framework_defaults_7_0.rb
      create  config/initializers/permissions_policy.rb
      create  config/locales
      create  config/locales/en.yml
      create  config/master.key
      append  .gitignore
      create  config/boot.rb
      create  config/database.yml
      create  db
      create  db/seeds.rb
      create  lib
      create  lib/tasks
      create  lib/tasks/.keep
      create  lib/assets
      create  lib/assets/.keep
      create  log
      create  log/.keep
      create  public
      create  public/404.html
      create  public/422.html
      create  public/500.html
      create  public/apple-touch-icon-precomposed.png
      create  public/apple-touch-icon.png
      create  public/favicon.ico
      create  public/robots.txt
      create  tmp
      create  tmp/.keep
      create  tmp/pids
      create  tmp/pids/.keep
      create  tmp/cache
      create  tmp/cache/assets
      create  vendor
      create  vendor/.keep
      create  test/fixtures/files
      create  test/fixtures/files/.keep
      create  test/controllers
      create  test/controllers/.keep
      create  test/mailers
      create  test/mailers/.keep
      create  test/models
      create  test/models/.keep
      create  test/helpers
      create  test/helpers/.keep
      create  test/integration
      create  test/integration/.keep
      create  test/channels/application_cable/connection_test.rb
      create  test/test_helper.rb
      create  test/system
      create  test/system/.keep
      create  test/application_system_test_case.rb
      create  storage
      create  storage/.keep
      create  tmp/storage
      create  tmp/storage/.keep
      remove  config/initializers/cors.rb
      remove  config/initializers/new_framework_defaults_7_0.rb
         run  bundle install
`/` is not writable.
Bundler will use `/tmp/bundler20220227-104-8kybut104' as your home directory temporarily.
Fetching gem metadata from https://rubygems.org/...........
Resolving dependencies...
Using rake 13.0.6
Using concurrent-ruby 1.1.9
Using minitest 5.15.0
Using builder 3.2.4
Using erubi 1.10.0
Using matrix 0.4.2
Fetching regexp_parser 2.2.1
Using rack 2.2.3
Fetching childprocess 4.1.0
Using websocket-extensions 0.1.5
Fetching io-console 0.5.11
Fetching public_suffix 4.0.6
Using racc 1.6.0
Using method_source 1.0.0
Using thor 1.2.1
Using zeitwerk 2.5.4
Using crass 1.0.6
Fetching rubyzip 2.3.2
Using nio4r 2.5.8
Fetching sqlite3 1.4.2
Using marcel 1.0.2
Using i18n 1.10.0
Using tzinfo 2.0.4
Using nokogiri 1.13.3 (x86_64-linux)
Using rack-test 1.1.0
Using mini_mime 1.1.2
Fetching sprockets 4.0.2
Using websocket-driver 0.7.5
Using activesupport 7.0.2.2
Using loofah 2.14.0
Fetching xpath 3.2.0
Using digest 3.1.0
Fetching puma 5.6.2
Using io-wait 0.2.1
Using rails-dom-testing 2.0.3
Using rails-html-sanitizer 1.4.2
Using globalid 1.0.0
Using activemodel 7.0.2.2
Using mail 2.7.1
Using rexml 3.2.5
Using actionview 7.0.2.2
Using activejob 7.0.2.2
Using activerecord 7.0.2.2
Using timeout 0.2.0
Fetching bindex 0.8.1
Fetching msgpack 1.4.5
Fetching jbuilder 2.11.5
Using bundler 2.3.3
Using actionpack 7.0.2.2
Using net-protocol 0.1.2
Using activestorage 7.0.2.2
Using railties 7.0.2.2
Using strscan 3.0.1
Using actioncable 7.0.2.2
Fetching net-imap 0.2.3
Using actiontext 7.0.2.2
Fetching importmap-rails 1.0.3
Using net-pop 0.1.1
Fetching stimulus-rails 1.0.4
Using net-smtp 0.3.1
Fetching turbo-rails 1.0.1
Installing regexp_parser 2.2.1
Installing puma 5.6.2 with native extensions
Installing childprocess 4.1.0
Installing rubyzip 2.3.2
Installing sprockets 4.0.2
Fetching selenium-webdriver 4.1.0
Fetching sprockets-rails 3.4.2
Installing msgpack 1.4.5 with native extensions
Installing sqlite3 1.4.2 with native extensions
Installing bindex 0.8.1 with native extensions
Installing jbuilder 2.11.5
Installing xpath 3.2.0
Fetching web-console 4.2.0
Installing io-console 0.5.11 with native extensions
Installing sprockets-rails 3.4.2
Installing net-imap 0.2.3
Using actionmailbox 7.0.2.2
Using actionmailer 7.0.2.2
Using rails 7.0.2.2
Installing importmap-rails 1.0.3
Installing stimulus-rails 1.0.4
Installing public_suffix 4.0.6
Fetching addressable 2.8.0
Installing web-console 4.2.0
Installing turbo-rails 1.0.1
Installing addressable 2.8.0
Fetching capybara 3.36.0
Fetching reline 0.3.1
Installing selenium-webdriver 4.1.0
Fetching webdrivers 5.0.0
Installing reline 0.3.1
Using irb 1.4.1
Using debug 1.4.0
Installing capybara 3.36.0
Installing webdrivers 5.0.0
Fetching bootsnap 1.10.3
Installing bootsnap 1.10.3 with native extensions
Bundle complete! 15 Gemfile dependencies, 74 gems now installed.
Use `bundle info [gemname]` to see where a bundled gem is installed.
         run  bundle binstubs bundler
       rails  importmap:install
Add Importmap include tags in application layout
      insert  app/views/layouts/application.html.erb
Create application.js module as entrypoint
      create  app/javascript/application.js
Use vendor/javascript for downloaded pins
      create  vendor/javascript
      create  vendor/javascript/.keep
Ensure JavaScript files are in the Sprocket manifest
      append  app/assets/config/manifest.js
Configure importmap paths in config/importmap.rb
      create  config/importmap.rb
Copying binstub
      create  bin/importmap
       rails  turbo:install stimulus:install
Import Turbo
      append  app/javascript/application.js
Pin Turbo
      append  config/importmap.rb
Run turbo:install:redis to switch on Redis and use it in development for turbo streams
Create controllers directory
      create  app/javascript/controllers
      create  app/javascript/controllers/index.js
      create  app/javascript/controllers/application.js
      create  app/javascript/controllers/hello_controller.js
Import Stimulus controllers
      append  app/javascript/application.js
Pin Stimulus
Appending: pin "@hotwired/stimulus", to: "stimulus.min.js", preload: true"
      append  config/importmap.rb
Appending: pin "@hotwired/stimulus-loading", to: "stimulus-loading.js", preload: true
      append  config/importmap.rb
Pin all controllers
Appending: pin_all_from "app/javascript/controllers", under: "controllers"
      append  config/importmap.rb
```

The hint from `git` about using `master` as the name for the initial
branch is harmless but you may want to go with the flow and rename the
branch to `main`.

``` sh
git branch -m main
```

> Note that any `git` configuration you may have set up in your $HOME
> directory is *not* available inside the container.

The `bundler` warning

``` sh
`/` is not writable.
Bundler will use `/tmp/bundler20220227-104-8kybut104' as your home directory temporarily.
```

is understandable and may be something to keep in mind to clean up if
left around.  When I checked, it was no longer there though.

Well then, let's not try to understand what all the stuff that was put
in place by `rails new` is all about right now and get going with

# Hello, Rails!

While the default

``` sh
bin/rails server
```

works, it restricts incoming requests to `localhost`.  This means that
only traffic originating from *within* the container will get a reply.
So running

``` sh
curl --head http://localhost:3000/
```

inside the container will result in a successfull HTTP request but
doing the same from the *outside*, i.e. the `localhost` where you started
the container as opposed to the `localhost` inside that container, you
get

``` console
olaf@basecamp:~/src/rails$ curl --head http://localhost:3000/
curl: (56) Recv failure: Connection reset by peer
```

To fix this we need to tell the server to bind to the container's IP
address or hostname.  Getting the IP address from inside the container
is next to impossible, all the tools I'd normally use to do so are not
installed, but using the hostname is trivial.

> I have since learnt that `hostname -i` returns the IP address and
> that this can be used inside a `ruby:latest` container.

``` sh
bin/rails server -b $HOSTNAME
```

does the trick.  Hitting the site now gives

``` console
olaf@basecamp:~/src/rails$ curl --head http://localhost:3000/
HTTP/1.1 200 OK
X-Frame-Options: SAMEORIGIN
X-XSS-Protection: 0
X-Content-Type-Options: nosniff
X-Download-Options: noopen
X-Permitted-Cross-Domain-Policies: none
Referrer-Policy: strict-origin-when-cross-origin
Content-Type: text/html; charset=utf-8
Vary: Accept
ETag: W/"698b7f7acea1643d7d55d7d96d346621"
Cache-Control: max-age=0, private, must-revalidate
Content-Security-Policy: script-src 'self' 'unsafe-inline'; style-src 'self' 'unsafe-inline'
X-Request-Id: b1aedad4-aebe-483d-88f5-60d9dd4153c5
X-Runtime: 0.084643
Server-Timing: sql.active_record;dur=1.83203125, start_processing.action_controller;dur=0.0888671875, !compile_template.action_view;dur=0.384765625, !render_template.action_view;dur=0.439453125, render_template.action_view;dur=0.948974609375, process_action.action_controller;dur=7.26904296875

```

so it works from *outside* the container too.  Good!

From here on, you can work your way through the [Getting Started](https://guides.rubyonrails.org/getting_started.html) Rails
Guide without any trouble.  There is no need for `sqlite3`, `node` and
`yarn` at all but let's have a go at adding these anyway.

# Adding SQLite3 and Node.js to the Mix

Exit the container, create and switch to another directory and restart
the container as before.  Well, almost.  I added my primary group to
the `-u` option.

``` sh
docker run --rm -it -v $PWD:/usr/src -w /usr/src \
    -u $(id -u):$(id -g) -p 3000:3000 ruby bash
```

Now, run

```
export | grep HOSTNAME
```

inside the container and open another terminal.  In that other
terminal copy the output of the above at the prompt and hit `Enter`.
This looks like

``` console
declare -x HOSTNAME="518c88d4366d"
```

but you will have another string of hex-digits.  With that done, run

``` sh
docker exec -it -u 0 $HOSTNAME bash
```

This should look a lot like

``` console
olaf@basecamp:~/src/rails$ docker exec -it -u 0 $HOSTNAME bash
root@518c88d4366d:/usr/src#
```

Note how this puts you in the same container you started at the
beginning of this section but as the `root` user.  Hence, you can
install extra packages with

``` sh
apt-get update
apt-get install --assume-yes --no-install-recommends \
    sqlite3 nodejs yarnpkg
```

and let it chug away.  The options are there to trim the list of
packages to install and make the command non-interactive.  You can
ignore upgradable packages because when you exit the container,
they'll be removed anyway.

The `yarnpkg` package provides the `yarn` command but *only* as `yarnpkg`.
You may want to add a `yarn` symlink yourself, like so

``` sh
ln -s /usr/bin/yarnpkg /usr/local/bin/yarn
```

Running `rails new .` (after you `gem install rails`!) in this setup
generates the *same* set of files.  Apart from the `credentials.yml.enc`
and `master.key` files in `config/`, all files are identical.

So, at least when starting out using the defaults, `sqlite3`, `node` and
`yarn` are not really required fare.  The project does use an SQLite3
database so you might want `sqlite3` if you have plans to muck around
with the DB directly.  For things like backing up and restoring the
schema that should be fine.  However, given that Rails models may be
doing a number of validations, I don't think modifying the DB using
`sqlite3` is the brightest of ideas.  Use the `rails console` and `rails
db:*` commands instead.

This holds for any database you use, not just SQLite3.

# Giving Bundler a HOME

Remember that `bundler` warning about not being able to write to `/` and
using a temporary directory?

``` console
I have no name!@518c88d4366d:/usr/src$ export HOME=$PWD
```

removes that warning but also leads to `bundle` creating a `.bundle/` in
`HOME`.  It had a 19MB `cache/` with `rubygems.org` info in it after `rails
new` completed.  The Gems below `GEM_HOME` are (re?)used.

``` console
I have no name!@518c88d4366d:/usr/src$ export GEM_HOME=$PWD/vendor/bundle
I have no name!@518c88d4366d:/usr/src$ gem install rails
I have no name!@518c88d4366d:/usr/src$ rails new .
```

Note that the `rails` invocation above uses `/usr/local/bundle/bin/rails`.
Additional Gems are installed in the new `GEM_HOME` location, good.

``` sh
I have no name!@518c88d4366d:/usr/src$ $GEM_HOME/bin/rails new .
```

works as well and the resultant scripts in `bin/` seem to do what they
are supposed to.

Combining the above and removing `BUNDLE_APP_CONFIG` from the mix (at
least until it's needed), we get

``` sh
unset BUNDLE_APP_CONFIG
export HOME=$PWD
export GEM_HOME=$HOME/vendor/bundle
gem install rails                  # /usr/local/bin/gem
export PATH=$GEM_HOME/bin:$PATH    # still includes /usr/local/bundle/bin
rails new .                        # uses $GEM_HOME/bin/rails
```

The above creates `.bundle/` and `.local/` directories with stuff we are
not interested in as far as version control is concerned.  Of course
the `GEM_HOME` should be ignored too.

While this works fine when setting things up, it's a bit annoying when
you keep popping in and out of the container as you need to muck a bit
with the environment variables every time you enter.  It's possible to
do so as part of the `docker` command invocation but gets a bit ugly

``` sh
docker run --rm -it -v $PWD:/usr/src -w /usr/src \
    -u $(id -u):$(id -g) -e HOME=/usr/src \
    -e GEM_HOME=/usr/src/vendor/bundle \
    -e PATH=/usr/src/vendor/bundle/bin:/usr/local/bin:/usr/bin:/bin \
    -p 3000:3000 ruby bash -c 'unset BUNDLE_APP_CONFIG; exec bash'
```

I have left out the various `sbin` locations in the `PATH` for brevity.
This should be mostly okay but they can easily be added back in if
needed.

Obviously, the above is something you probably want to stick in a
shell alias or script rather than memorize.  I'd suggest a pair of
scripts, one to start a command line in the container, another to
run `bin/rails serve` inside the container.
