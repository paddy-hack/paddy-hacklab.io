---
SPDX-License-Identifier: CC-BY-SA-4.0
SPDX-FileCopyrightText: © 2022 Olaf Meeuwissen
title: "GitLab SaaS Free Tier Getting Restrictive"
layout: post
---

On 2022-09-06 I got an email informing me of upcoming changes to the
[GitLab SaaS Free Tier](https://about.gitlab.com/pricing/).  They would affect user and storage limits on
or after 2022-10-19.  I had seen earlier announcements of changes but
never paid much attention as they did not seem to affect me.  This
time, the storage limits would affect me so I took a deep dive.

# CI/CD Minutes

The first change announcements I remember are the ones changing CI/CD
minutes.  First, *new* free users would be [limited to 2000 minutes](https://about.gitlab.com/blog/2020/03/18/ci-minutes-for-free-users/) per
group per month, effective 2020-03-15.  Existing free users, like
myself (since 2015-04-17), would not be affected.

Next, effective 2020-01-01, users on the Free Tier would be [limited to
400 minutes](https://about.gitlab.com/blog/2020/09/01/ci-minutes-update-free-users/) per top-level group or personal namespace per month.  That
might have affected me if it weren't for the fact that my pipelines do
not consume that much.  Not even close, most of the time.

Then, as of 2021-05-17, *new* free users would need to [register a credit
card](https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/) to use GitLab's shared runners in an attempt to fight cryptominer
abuse.  Again, existing users were not affected.  In an update,
effective 2021-07-17, public project CI/CD minutes were included in
the [quota](https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html) for *new* namespaces, but these accumulate at a slower [rate](https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#cost-factor).

> There have been other updates to this requirement.  See the linked
> blog post for details.

Checking my top-level groups and personal namespace, I found my newer
ones are at 400 minutes per months while the older ones are still at
2000 minutes.  With my current usage, I don't think I'll ever hit any
of my limits.  In the unlikely situation that I do however, USD10 for
1000 minutes is afforable.  Unused [purchased minutes](https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#purchase-additional-cicd-minutes) carry over for up
to a year.

There is one thing I find confusing in the CI/CD minutes reporting
though and that is the difference between duration and usage.  The
former is wall-clock time for shared runner jobs.  The latter is
duration multiplied by a [rate](https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#cost-factor), where rates depends on the kind and
visibility of the project.

> I racked up 4000-plus duration minutes in 2022-06 working on a fork
> of the `omnibus-gitlab` project but that amounted to fewer than 35
> usage minutes!

# No More GitLab Ultimate Benefits As Of 2022-07-01

This came in the form of in-app notifiactions and is fine with me.
Actually, I prefer it.  I like to keep my own projects [Free As In
Freedom](https://www.gnu.org/philosophy/free-sw.html) and entanglement with what is offered by GitLab functionality
backed by source code that is kept under wraps is something I have
been trying to avoid.  This change makes that easier so losing
"Ultimate Benefits" is beneficial for me.

> It would be even better if all those "benefits" are kept out of the
> web UI as well.  I would also like to keep that "Start an Ultimate
> trial" item out of my pull-down menu, by the way.

# User Limits

Starting 2022-10-19, there will be a [five user limit](https://about.gitlab.com/blog/2022/03/24/efficient-free-tier/) on top-level
namespaces.  The blog post is a bit confusing in that it states that
this does not apply to "top-level namespaces with public visibility"
but mentions "top-level namespaces with only public projects" in the
update.

> Wondering how this works for top-level namespaces that have *all*
> their projects in subgroups.  Assuming the statement applies
> recursively and that even a *single* non-public, i.e. private or
> internal, project at any level makes it subject to the limit.

As of writing, I am no longer involved as a member in any Open Source
projects but five users is a *very* low limit.  I know these projects
can apply to the [Open Source program](https://about.gitlab.com/solutions/open-source/) but the *annual* red tape is not
encouraging.  Not to mention GitLab's right to accept projects and
change or terminate the program at their sole discretion.

Anyway, my own projects max out at two users at the moment, so the
limit is not a problem for me (yet?).  It might be nice though to keep
the Bronze/Starter tier that is being [phased out](https://about.gitlab.com/blog/2021/01/26/new-gitlab-product-subscription-model/) so hobbyist/volunteer
kind of projects have an affordable alternative when they hit the five
user limit.  A Silver/Premium subscription at USD19 per user per month
is prohibitively expensive.  Something at four or five USD is doable,
I think.

> To put that Premium subscription into perspective a bit, it is more
> expensive than what I [donate to the Free Software Foundation](https://my.fsf.org/donate) every
> year.

# Storage Limits

As mentioned in the beginning, this came to me via email and I haven't
been able to find a blog post for this change.  The [GitLab SaaS Free
Tier FAQ](https://about.gitlab.com/pricing/faq-efficient-free-tier/) has details though.  The Free Tier is to limit storage to 5GB
per top-level namespace.  Considering that this includes packages, the
container registry and the dependency proxy, that is not an awful lot.
The FAQ only mentions other tiers as alternatives, with Premium at
USD19 per user per month with 50GB of storage, but the [pricing page](https://about.gitlab.com/pricing/)
lists an add-on at USD60 per 10GB per year.  I still think that is a
pretty steep price to pay but it is a lot more reasonable than paying
for storage on a per user basis.

Now let's see how bad things are for me.  First my top-level groups.
I have five of those.  One is in the process of getting removed and
another is just getting up to speed.  A third only contains small
projects to customize my `$HOME` directory.  The fourth has not been
getting much attention for a while now.  All four are (still) very
small in terms of storage usage.  The fifth and last project uses
about 1GB so should be okay for a while to come.

My personal namespace, at 57.3 GiB, is in trouble though.  The bulk,
57.1 GiB, is in container registries.  This comes about because I am
running scheduled pipelines to keep a few public container images
up-to-date and keep all previous versions for historic purposes.  I
guess that will no longer do.  Unless I do something about this, I
will no longer be able to do much in my personal namespace.  Like, for
example, push blog posts like this one to the repository backing the
page you are reading right now.

> Storage Usage Quota seriously sucks, as of writing.  The per project
> storage usage does not include the container registry.  Instead all
> container registry storage is lumped into a single total leaving me
> to chase down which projects are "storage hogs".  My projects don't
> use packages or dependency proxies but I'm wondering how those are
> reported.

I could move these storage hogging projects into their own top-level
namespace.  That would most certainly create space in my personal
namespace.  However, at 22.4GiB and 30.3GiB respectively, those new
top-level namespaces would immediately be blocked from updates.

> At least, I thought I could move them.  Giving it a try, I promptly
> see a `Project cannot be transferred, because tags are present in its
> container registry`.  So much for that idea.

Unwilling to pay upwards of USD300 per year for storage for these two
projects, I could stop keeping all previous versions and only keep a
few.  Or try to find an alternative location to host the images.  The
[Docker Hub](https://www.docker.com/pricing/) has *unlimited* public image repositories for USD0 per month
for example (but see [this issue](https://github.com/docker/hub-feedback/issues/373) for why I find that sub-par for me).
Dropping the projects altogether and switching to similar images
maintained by others might be another option.

Not having access to historic registry download statistics, over say
the last year, makes it hard to decide what to do.  I started pulling
everything to local storage for now and will clean out the GitLab.com
side after that.

# Conclusion

Considering all these restrictions, it rather looks like GitLab.com is
morphing its Free Tier into a playground for people to give it a spin
in the hope they sign up to a paid tier.  I'm not sure I like where
that is heading but for now I'll see how far the Free Tier lets me do
what I want to.
