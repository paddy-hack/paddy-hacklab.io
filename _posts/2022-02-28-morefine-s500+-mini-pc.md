---
SPDX-License-Identifier: CC-BY-SA-4.0
SPDX-FileCopyrightText: © 2022 Olaf Meeuwissen
layout: post
title: "MOREFINE S500+ Mini PC"
---

> This post talks about purchases I made end November, begin December
> 2021.

With COVID-19 cancelling all the marathons and trail races I would
normally spend it on, my allowance had ballooned to a seizable sum.
At the same time, my desktop machine was aging badly; dual core, 4GB
of memory.  It just so happened that I chanced across an add for an
[Indiegogo campaign](https://www.indiegogo.com/projects/morefine-s500-powerful-amd-ryzen-9-5900hx-mini-pc) while reading a BBC News article on our Amazon Fire
tablet.  Normally, I ignore any adds on that device (my other devices
are tweaked to not show them to begin with), but this one triggered my
curiosity.  While the hardware looked good, the backer comments didn't
quite so.  In the comments, though, I found a [link](https://www.aliexpress.com/item/1005003697306243.html) to another vendor,
KINGDEL Technology, Inc., on [Aliexpress](https://www.aliexpress.com/) offering pretty much the same
hardware.  Further searching at Aliexpress turned up a very similar
[offer by Topton](https://www.aliexpress.com/item/1005003398302546.html) as well as an [offer by MOREFINE](https://www.aliexpress.com/item/1005003349714611.html), instigators of the
Indiegogo campaign, itself.  Comparing the various vendors, I finally
chose [Topton](http://www.toptonminipc.com/) because of the substantially larger amount of feedback.

> I tracked down a [product link](https://morefines.com/products/morefine-s500-mini-pc) on the [English MOREFINE site](https://morefines.com/) ([中文](http://morefineglobal.com/)).

BTW, this was the first time I ordered via Aliexpress.  I was a bit
hesitant about that, having only ordered on-line from Amazon (US,
Japan and Germany) up until then, but everything went smoothly.

I went for a configuration with

* 2TB NVMe SSD
- 64GB DDR4 RAM
* an Intel AX200 option
* Ubuntu pre-installed

Vendor feedback on my questions via Aliexpress was curt (and a wee bit
vague) but prompt.  Delivery was prompt too, after factoring in the up
to 15 days before shipping that I somehow completely didn't see while
going through the ordering process.  Even so, my order arrived in 14
days (spending only four of those in transit from Hong Kong to Japan,
a good deal less than the nine day DHL delivery advertised).

My order arrived neatly packed and without any damage.  All the ports
are well aligned with the casing so there is no trouble plugging in
cables.  I had asked for a pre-install with Ubuntu and that's what I
got but I was less impressed with the Chinese-only login screen and a
lack of information on the user account.  Turns out the prepared user
account is `user` but I had to ask for the password.  While the reply
came swiftly, I would have appreciated having account info documented
with the order.

I was able to boot fine from a USB stick with a Devuan Chimaera live
desktop. You have to press the `ESC` key real quick to get at the GRUB
boot menu (also in Chinese) and select the `UEFI Firmware Settings` to
get at the BIOS where you can override the boot medium at the `Save &
Exit` tab.  From there, I [explored the hardware and the pre-installed
Ubuntu]({% link _posts/2022-03-08-exploring-my-morefine-s500+.md %}).

Booting from a USB stick allows me to change language settings and the
`user` password in the pre-installed system, if so inclined, but that's
only because I know my way around Linux systems (after two decades of
tinkering).

I gave only four stars instead of five in my [feedback](https://www.aliexpress.com/item/1005003398302546.html) because the PC
came without information on the `user` account name and password for
the pre-installed OS and was configured for Chinese without any means
to change it from the login screen.

# Additional Hardware

Seeing that my brand new mini PC would neither support my old VGA and
DVI monitors nor my PS/2 keyboard, I went in search of a monitor and
keyboard.  Only my wired USB mouse would still be usable but when I
started looking at wireless keyboards, I figured I might as well round
things out and get a wireless mouse too.

After a good deal of searching over several days, I settled on

- a [Dell S3221QS](https://dell.com/S3221QS) 31.5 inch non-glare monitor ([日本語](https://www.dell.com/ja-jp/shop/dell-s3221qs-315%E3%82%A4%E3%83%B3%E3%83%81%E3%83%AF%E3%82%A4%E3%83%89%E3%83%A2%E3%83%8B%E3%82%BF%E3%83%BC4k-%E6%9B%B2%E9%9D%A2-%E9%9D%9E%E5%85%89%E6%B2%A2-hdr-hdmix2dp-%E9%AB%98%E3%81%95%E8%AA%BF%E6%95%B4-%E3%82%B9%E3%83%94%E3%83%BC%E3%82%AB%E3%83%BC-freesync/apd/210-axhq/%E3%83%A2%E3%83%8B%E3%82%BF%E3%83%BC-%E3%83%A2%E3%83%8B%E3%82%BF%E3%83%BC%E3%82%A2%E3%82%AF%E3%82%BB%E3%82%B5%E3%83%AA%E3%83%BC))
- an [iClever IC-BK22](https://www.iclever.co.jp/products-bluetooth-keyboard-bk22-bk) wireless keyboard (in Japanese)
- an [iClever MD172](https://www.iclever.co.jp/products-mouse-md172) wireless mouse (in Japanese)

All three were ordered from [Amazon Japan](https://www.amazon.co.jp).

The monitor is huge to compared what I've been using so far but it
very nicely supports a tiled window manager layout with three columns.
I typically have a few vertically stacked command line terminal on the
left, one or two editor windows in the middle and one or two browsers
on the right.  Works like a charm.

The keyboard is Bluetooth-only, whereas the mouse also supports
wireless communication via a USB adapter.  The nice thing about them
is that they are USB rechargeable; no need for batteries anymore.
Unfortunately, they use different USB connectors so I really need to
hang onto both included cables.

The keyboard can be paired with up to three machines, the mouse too
although one of those connections would use the wireless USB adapter
instead of Bluetooth.

The keyboard is a bit bouncy.  If I hit the keys a tad forcefully, I
get doubling of the characters I typed.  This is probably due to the
keyboard's thinness.  I guess just have to get used to typing more
gently (and not vent any frustration via the keyboard).

The mouse is a bit bigger that I had become used to, even though I
ordered the medium size.  In hindsight, I should have ordered the
small one.  Oh well.

All in all, I am one happy hacker with my new hardware.
