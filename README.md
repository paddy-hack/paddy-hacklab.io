---
SPDX-License-Identifier: CC-BY-SA-4.0
SPDX-FileCopyrightText: © 2022 Olaf Meeuwissen
---

# cave@lector _(`paddy-hack.gitlab.io`)_

This project is used to maintain and publish my collected "ramblings".

You can find the published collection at https://paddy-hack.gitlab.io

## Contributing

Star this project if you like it.  If you want to comment or have a
question, submit an issue.  Merge requests for factual mistakes, bad
grammar and typos are welcome.

## License

[CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/) © [Olaf Meeuwissen](https://gitlab.com/paddy-hack)
